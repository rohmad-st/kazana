<?php

namespace App;

use App\Kazana\Helper;
use Illuminate\Database\Eloquent\Model;
use Silvanite\Brandenburg\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    use HasRoles;
    use SoftDeletes;

    protected $dates = [
      'deleted_at',
    ];

    public function pondok()
    {
        return $this->belongsTo('App\Pondok', 'pondok_id', 'id');
    }

    public function merchant_category()
    {
        return $this->belongsTo('App\MerchantCategory', 'merchant_category_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($merchant) {
        	if ( !$merchant->otp )
        	{
        		$merchant->otp = Helper::generatePIN();
        	}

            if ( \Auth::check() && \Auth::user()->pondok_id > 0 )
            {
                $merchant->pondok_id = \Auth::user()->pondok_id;
            }            
        });
    }
}
