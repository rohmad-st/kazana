<?php

namespace App\Nova\Actions;

use App\Santri;
use Laravel\Nova\Fields\Text;
use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Laravel\Nova\Actions\DestructiveAction;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteSantriData extends Action // DestructiveAction
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $total = 0;
        foreach ($models as $model) 
        {
            Santri::where('id', '=', $model->id)->take(1)->update(['status' => 2, 'deleted_reason' => $fields->reason]);

            $total++;
        }
        
        return Action::message('Delete '. $total .' Santri. Alasan : '. $fields->reason);
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Reason')->rules('required'),
        ];
    }
}
