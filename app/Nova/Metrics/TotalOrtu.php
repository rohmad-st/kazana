<?php

namespace App\Nova\Metrics;

use App\Ortu;
use Illuminate\Http\Request;
use Laravel\Nova\Metrics\Value;

class TotalOrtu extends Value
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function calculate(Request $request)
    {
        if ( \Auth::user()->pondok_id > 0 )
        {
            $total = Ortu::where('pondok_id', \Auth::user()->pondok_id)->where('status', 1)->count();
        }
        else
        {
            $total = Ortu::where('status', 1)->count();
        }
        return $this->result($total);
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            //30 => '30 Days',
            //60 => '60 Days',
            //365 => '365 Days',
            //'MTD' => 'Month To Date',
            //'QTD' => 'Quarter To Date',
            //'YTD' => 'Year To Date',
        ];
    }

    /**
     * Get the displayable label of the metrics.
     *
     * @return string
     */
    public $name = 'Total Orang Tua';

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        //return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'total-ortu';
    }
}
