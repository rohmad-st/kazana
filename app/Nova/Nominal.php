<?php

namespace App\Nova;

use App\Kazana\Helper;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Kristories\Qrcode\Qrcode;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;


class Nominal extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Nominal';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'value';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'value',
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Nominal Barcode';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Number::make('Value')->rules('required'),
            Select::make('Status')->options([
                '1' => 'Active',
                '0' => 'Not Active',
            ])->rules('required')->hideFromIndex(),
            Qrcode::make('QR Code')
                ->text($this->barcode)
                ->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        if ( Helper::isAdmin() === false )
        {
            return [
                (new DownloadExcel)->withFilename('nominal-' . date('dmY-His') . '.xlsx')
                    ->withHeadings()
                    //->only('merchant_name', 'username'),
                    ->allFields()->except('barcode', 'pin', 'image', 'deleted_at'),
            ];
        }
        else
        {
            return [
                (new DownloadExcel)->withFilename('nominal-' . date('dmY-His') . '.xlsx')
                    ->withHeadings()
                    //->only('merchant_name', 'username'),
                    ->allFields()->except('barcode', 'pin', 'image', 'deleted_at'),
            ];
        }
    }
}
