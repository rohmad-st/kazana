<?php

namespace App\Nova;

use App\Kazana\Helper;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Text;
use Kristories\Qrcode\Qrcode;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\BelongsTo;
use App\Nova\Actions\DeleteSantriData;
use App\Kazana\XlsReader\SantriXlsReader;
use App\Kazana\XlsHandler\SantriXlsHandler;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Santri extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Santri';

    public static $importFileReader = SantriXlsReader::class;
    public static $importHandler = SantriXlsHandler::class;
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Santri';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new Panel('Informasi Utama', $this->personalFields()),
            new Panel('Informasi Pribadi', $this->biodataFields()),
            new Panel('Sekolah Asal', $this->fromSchoolFields()),
            new Panel('Ujian Nasional', $this->ujianNasionalFields()),
            new Panel('Asrama Pondok Pesantren', $this->pondokPesantrenFields()),
            new Panel('Lain - lain', $this->otherFields()),
        ];
    }

    /**
     * Get the biodata fields for the resource.
     *
     * @return array
     */
    protected function personalFields()
    {
        if (Helper::isAdmin())
        {
            $status_opt = Select::make('Status')
                ->options([
                    0 => 'Tidak Aktif',
                    1 => 'Aktif',
                    2 => 'Pengajuan dihapus',
                ])->displayUsingLabels();
                // ->rules('required');
        }
        else
        {
            $status_opt = Text::make('Status')->readonly(function ($request) {
                    return true;
                })
                ->withMeta([
                        'value' => $this->status ? 'Aktif' : 'Tidak Aktif',
                ]);
        }

        $arr_objects = [
            ID::make()->sortable(),
            Text::make('Name')->rules('required')->sortable(),
            Text::make('Nomor Induk', 'nisn')->rules('required')->sortable(),
            Text::make('MVA', 'no_virtual_account')->withMeta($this->no_virtual_account ? [] : ["value" => "88986"
            ,'extraAttributes' => [
                'disabled' => true,
           ]])            
            ->hideFromIndex(),
            Number::make('Saldo')->hideFromIndex(),
            Number::make('Limit Harian', 'transaction_daily_limit')->hideFromIndex(),
            Password::make('Pin')->onlyOnForms()
                    ->creationRules('required', 'string', 'min:6,max:6')
                    ->updateRules('nullable', 'string', 'min:6,max:6'),
            BelongsTo::make('Sekolah', 'pondok', 'App\Nova\Pondok')->rules('required')
            ->withMeta([
                'belongsToId' => $this->pondok_id ?? auth()->user()->pondok_id
            ]),
        ];

        array_push($arr_objects, $status_opt);

        if ( isset($this->deleted_reason) && $this->deleted_reason)
        {
            $deleted_reason_opt = Text::make('Pengajuan Penghapusan', 'deleted_reason')->readonly(function ($request) {
                    return true;
                })->onlyOnDetail();
            array_push($arr_objects, $deleted_reason_opt);
        }

        return $arr_objects;
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function biodataFields()
    {
        return [
            Text::make('Tempat Lahir', 'tempat_lahir')->hideFromIndex(),
            Date::make('Tanggal Lahir', 'tanggal_lahir')->hideFromIndex(),
            Select::make('Jenis Kelamin', 'kelamin')->options([
                1 => 'Male',
                0 => 'Female',
            ])->rules('required')->hideFromIndex(),            
            Number::make('Anak ke', 'anak_ke')->hideFromIndex(),
            Number::make('Jumlah Saudara', 'jumlah_saudara')->hideFromIndex(),
            Select::make('Gologan Darah', 'golongan_darah')->options([
                'o' => 'O',
                'a' => 'A',
                'b' => 'B',
                'ab' => 'AB',
            ])->hideFromIndex(),            

            Text::make('Phone')->rules('required')->hideFromIndex(),
            Text::make('Address')->rules('required')->hideFromIndex(),
            Trix::make('Description')->withFiles('public'),
            Avatar::make('Image')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function fromSchoolFields()
    {
        return [
            Text::make('Sekolah Asal', 'asal_sekolah')->hideFromIndex(),
            Text::make('Alamat Sekolah', 'asal_sekolah_alamat')->hideFromIndex(),
            Text::make('Desa Sekolah Asal', 'asal_sekolah_desa')->hideFromIndex(),
            Text::make('Kecamatan Sekolah Asal', 'asal_sekolah_kecamatan')->hideFromIndex(),
            Text::make('Kota Sekolah Asal', 'asal_sekolah_kota')->hideFromIndex(),
            Text::make('Provinsi Sekolah Asal', 'asal_sekolah_provinsi')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function ujianNasionalFields()
    {
        return [
            Text::make('Nomor UN', 'nomor_un')->hideFromIndex(),
            Text::make('Nomor SKHUN/ SKHUAMEN', 'nomor_skhun')->hideFromIndex(),
            Text::make('Mata Pelajaran', 'mata_pelajaran')->hideFromIndex(),
            Text::make('Nilai', 'nilai')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function pondokPesantrenFields()
    {
        return [
            Text::make('Nama Pondon Pesantren', 'nama_pondok')->hideFromIndex(),
            Text::make('Nama Kamar', 'nama_kamar')->hideFromIndex(),
            Boolean::make('Punya Saudara Kandung di Ponpes?', 'punya_saudara_ponpes')->hideFromIndex(),
            Number::make('Jumlah Saudara Kandung di Ponpes?', 'jumlah_saudara_ponpes')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function otherFields()
    {
        return [
            Text::make('Nama Sekolah/ Madrasah', 'nama_sekolah')->hideFromIndex(),
            Select::make('Diterima di Kelas', 'diterima_dikelas')->options([
                'a' => 'I/VII/X',
                'b' => 'II/VIII/XI',
                'c' => 'III/IX/XII',
                'd' => 'IV',
                'e' => 'V',
                'f' => 'VI',
            ])->hideFromIndex(),            
            Text::make('Nama Prestasi', 'prestasi_nama')->hideFromIndex(),
            Select::make('Tingkat Prestasi', 'prestasi_tingkat')->options([
                'kota' => 'Kotamadya/ Kabupaten',
                'provinsi' => 'Provinsi',
                'nasional' => 'Nasional',
            ])->hideFromIndex(),            
            Text::make('Tahun Prestasi', 'prestasi_tahun')->hideFromIndex(),
            Select::make('Sumber Informasi', 'sumber_informasi')->options([
                'televisi' => 'Televisi',
                'internet' => 'Internet/ Website',
                'radio' => 'Radio',
                'majalan' => 'Majalah / Koran',
                'keluarga' => 'Keluarga Dekat',
                'teman' => 'Teman',
                'tetangga' => 'tetangga',
                'lain' => 'Lainnya',
            ])->hideFromIndex(),            
        ];
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */    
    public static function indexQuery(NovaRequest $request, $query)
    {
        // filter by user id
        if ( \Auth::user()->pondok_id > 0 )
        {
            $query->where('pondok_id', '=', \Auth::user()->pondok_id);
        }

        if (Helper::isAdmin() === false)
        {
            $query->whereIn('status', [0, 1]);
        }
    }
    
    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        // filter by user id
        if ( \Auth::user()->pondok_id > 0 )
        {
            return [
                new \Sparclex\NovaImportCard\NovaImportCard(Santri::class),
                new \Kazana\SantriImportPhoto\SantriImportPhoto,
            ];
        }
        else
        {
            return [];
        }
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        if ( Helper::isAdmin() === false )
        {
            return [
                (new DownloadExcel)->withFilename('santri-' . date('dmY-His') . '.xlsx')
                    ->withHeadings()
                    ->allFields()->except('barcode', 'pin', 'image', 'deleted_at'),
                (new DeleteSantriData)->canSee(function ($request) {
                    return true;
                })->canRun(function ($request, $user) {
                    return true;
                }),
            ];
        }
        else
        {
            return [
                (new DownloadExcel)->withFilename('santri-' . date('dmY-His') . '.xlsx')
                    ->withHeadings()
                    ->allFields()->except('barcode', 'pin', 'image', 'deleted_at'),
            ];
        }
    }
}
