<?php

namespace App\Nova;

use Laravel\Nova\Panel;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use App\Kazana\XlsReader\PondokXlsReader;
use App\Kazana\XlsHandler\PondokXlsHandler;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Pondok extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Pondok';

    public static $importFileReader = PondokXlsReader::class;
    public static $importHandler = PondokXlsHandler::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Sekolah';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new Panel('Informasi Utama', $this->personalFields()),
            new Panel('Alamat Ponpes', $this->addressPondokFields()),
            new Panel('Kontak Ponpes', $this->contactPondokFields()),
            new Panel('Kontak Kepala Ponpes', $this->contactKepalaPondokFields()),
            new Panel('Kontak Pimpinan Yayasan', $this->contactKepalaYayasanFields()),
        ];
    }

    /**
     * Get the biodata fields for the resource.
     *
     * @return array
     */
    protected function personalFields()
    {
        return [
            ID::make()->sortable(),
            Text::make('NSPP')->rules('required')->sortable(),
            Text::make('Name')->rules('required')->sortable(),
            Number::make('Phone')->rules('required'),
            Trix::make('Description')->rules('required')->withFiles('public'),
            BelongsTo::make('City')->rules('required'),
            Avatar::make('Image')->hideFromIndex(),
            Boolean::make('Status')->rules('required'),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function addressPondokFields()
    {
        return [
            Text::make('Address')->rules('required')->hideFromIndex(),
            Text::make('Desa')->hideFromIndex(),
            Text::make('Kecamatan')->hideFromIndex(),
            Text::make('Kota')->hideFromIndex(),
            Text::make('Provinsi')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function contactPondokFields()
    {
        return [
            Text::make('Fax')->hideFromIndex(),
            Text::make('Email')->hideFromIndex(),
            Text::make('Website')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function contactKepalaPondokFields()
    {
        return [
            Text::make('Nama Kepala Ponpes', 'ponpes_kepala')->hideFromIndex(),
            Text::make('Alamat Kepala Ponpes', 'ponpes_alamat')->hideFromIndex(),
            Text::make('Telepon Kepala Ponpes', 'ponpes_telepon')->hideFromIndex(),
            Text::make('Email Kepala Ponpes', 'ponpes_email')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function contactKepalaYayasanFields()
    {
        return [
            Text::make('Nama Pimpinan Yayasan', 'yayasan_nama')->hideFromIndex(),
            Text::make('Alamat Pimpinan Yayasan', 'yayasan_alamat')->hideFromIndex(),
            Text::make('Telepon Pimpinan Yayasan', 'yayasan_telepon')->hideFromIndex(),
            Text::make('Email Pimpinan Yayasan', 'yayasan_email')->hideFromIndex(),
        ];
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */    
    public static function indexQuery(NovaRequest $request, $query)
    {
        // filter by user id
        if ( \Auth::user()->pondok_id > 0 )
        {
            $query->where('id', '=', \Auth::user()->pondok_id);
        }
        
        //const DEFAULT_INDEX_ORDER = 'last_name';
        /*$query->when(empty($request->get('orderBy')), function(Builder $q) {
            $q->getQuery()->orders = [];

            return $q->orderBy(static::DEFAULT_INDEX_ORDER);
        });*/
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        if ( \Auth::user()->pondok_id == 0 )
        {
            return [
                new \Sparclex\NovaImportCard\NovaImportCard(Pondok::class),
            ];            
        }
        else
        {
            return [];
        }
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel)->withFilename('pondoks-' . date('dmY-His') . '.xlsx')
                ->withHeadings()
                //->only('merchant_name', 'username'),
                ->allFields()->except('image', 'deleted_at'),
        ];
    }
}
