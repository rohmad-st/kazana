<?php

namespace App\Nova;

use Laravel\Nova\Panel;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Kazana\XlsReader\MerchantXlsReader;
use App\Kazana\XlsHandler\MerchantXlsHandler;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Merchant extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Merchant';

    public static $importFileReader = MerchantXlsReader::class;
    public static $importHandler = MerchantXlsHandler::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'merchant_name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'merchant_name',
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Merchant';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new Panel('Informasi Utama', $this->personalFields()),
            new Panel('Alamat Merchant', $this->addressFields()),
            new Panel('Kontak Merchant', $this->contactFields()),
            new Panel('Kontak Pimpinan', $this->contactPimpinanFields()),
            new Panel('Kontak Pemilik', $this->contactPemilikFields()),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function personalFields()
    {
        return [
            ID::make()->sortable(),
            Text::make('No Registrasi', 'no_registrasi')->rules('required')->sortable(),
            BelongsTo::make('Pondok')->rules('required'),
            BelongsTo::make('Merchant Category')->rules('required'),
            Text::make('Merchant Name', 'merchant_name')->rules('required')->sortable(),
            Number::make('Saldo')->rules('required')->sortable(),
            Text::make('No Rekening', 'no_rekening')->hideFromIndex(),
            Text::make('Username')->rules('required')->hideFromIndex(),
            Password::make('Password')->onlyOnForms()
                    ->creationRules('required', 'string', 'min:6,max:6')
                    ->updateRules('nullable', 'string', 'min:6,max:6'),
            Text::make('First Name', 'first_name')->hideFromIndex()->rules('required'),
            Text::make('Last Name', 'last_name')->hideFromIndex()->rules('required'),
            Number::make('Phone')->rules('required')->hideFromIndex(),
            Trix::make('Description')->withFiles('public'),
            Avatar::make('Image')->hideFromIndex(),
            Select::make('Status')->options([
                '1' => 'Active',
                '0' => 'Not Active',
            ])->rules('required')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function addressFields()
    {
        return [
            Text::make('Alamat', 'alamat')->hideFromIndex(),
            Text::make('Desa', 'desa')->hideFromIndex(),
            Text::make('Kecamatan', 'kecamatan')->hideFromIndex(),
            Text::make('Kota', 'kota')->hideFromIndex(),
            Text::make('Provinsi', 'provinsi')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function contactFields()
    {
        return [
            Text::make('Fax', 'fax')->hideFromIndex(),
            Text::make('Email', 'email')->hideFromIndex(),
            Text::make('Website', 'website')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function contactPimpinanFields()
    {
        return [
            Text::make('Nama Pimpinan', 'pimpinan_nama')->hideFromIndex(),
            Text::make('Alamat Pimpinan', 'pimpinan_alamat')->hideFromIndex(),
            Text::make('Desa Pimpinan', 'pimpinan_desa')->hideFromIndex(),
            Text::make('Kecamatan Pimpinan', 'pimpinan_kecamatan')->hideFromIndex(),
            Text::make('Kota Pimpinan', 'pimpinan_kota')->hideFromIndex(),
            Text::make('Provinsi Pimpinan', 'pimpinan_provinsi')->hideFromIndex(),
            Text::make('Kodepos Pimpinan', 'pimpinan_kodepos')->hideFromIndex(),
            Text::make('Telepon Pimpinan', 'pimpinan_phone')->hideFromIndex(),
            Text::make('Email Pimpinan', 'pimpinan_email')->hideFromIndex(),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function contactPemilikFields()
    {
        return [
            Text::make('Nama Pemilik', 'pemilik_nama')->hideFromIndex(),
            Text::make('Alamat Pemilik', 'pemilik_alamat')->hideFromIndex(),
            Text::make('Desa Pemilik', 'pemilik_desa')->hideFromIndex(),
            Text::make('Kecamatan Pemilik', 'pemilik_kecamatan')->hideFromIndex(),
            Text::make('Kota Pemilik', 'pemilik_kota')->hideFromIndex(),
            Text::make('Provinsi Pemilik', 'pemilik_provinsi')->hideFromIndex(),
            Text::make('Kodepos Pemilik', 'pemilik_kodepos')->hideFromIndex(),
            Text::make('Telepon Pemilik', 'pemilik_phone')->hideFromIndex(),
            Text::make('Email Pemilik', 'pemilik_email')->hideFromIndex(),
        ];
    }


    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */    
    public static function indexQuery(NovaRequest $request, $query)
    {
        // filter by user id
        if ( \Auth::user()->pondok_id > 0 )
        {
            $query->where('pondok_id', '=', \Auth::user()->pondok_id);
        }
        
        //const DEFAULT_INDEX_ORDER = 'last_name';
        /*$query->when(empty($request->get('orderBy')), function(Builder $q) {
            $q->getQuery()->orders = [];

            return $q->orderBy(static::DEFAULT_INDEX_ORDER);
        });*/
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        // filter by user id
        if ( \Auth::user()->pondok_id > 0 )
        {
            return [
            	new \Sparclex\NovaImportCard\NovaImportCard(Merchant::class),
            ];
        }
        else
        {
            return [];
        }
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
        	(new DownloadExcel)->withFilename('merchant-' . date('dmY-His') . '.xlsx')
        		->withHeadings()
        		//->only('merchant_name', 'username'),
        		->allFields()->except('password', 'image', 'otp', 'token', 'deleted_at'),
        ];
    }
}
