<?php

namespace App\Nova;

use Laravel\Nova\Panel;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\BelongsTo;
use App\Kazana\XlsReader\OrtuXlsReader;
use App\Kazana\XlsHandler\OrtuXlsHandler;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Ortu extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Ortu';

    public static $importFileReader = OrtuXlsReader::class;
    public static $importHandler = OrtuXlsHandler::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Orang Tua';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new Panel('Informasi Utama', $this->personalFields()),
            new Panel('Informasi Ayah', $this->fatherFields()),
            new Panel('Informasi Ibu', $this->motherFields()),
            new Panel('Informasi Lainnya', $this->otherFields()),
        ];
    }

    /**
     * Get the biodata fields for the resource.
     *
     * @return array
     */
    protected function personalFields()
    {
        return [
            ID::make()->sortable(),
            Text::make('Phone')->rules('required')->hideFromIndex(),
            Text::make('Username')->rules('required')->sortable(),
            Password::make('Password')->onlyOnForms()
                    //->withMeta(['value' => ''])
                    ->creationRules('required', 'string', 'min:6')
                    ->updateRules('nullable', 'string', 'min:6'),
            BelongsTo::make('Pondok')->rules('required'),
            BelongsTo::make('Santri')->rules('required'),
            Select::make('Status')->options([
                '1' => 'Active',
                '0' => 'Not Active',
            ])->rules('required')->hideFromIndex(),
        ];
    }


    /**
     * Get the biodata fields for the resource.
     *
     * @return array
     */
    protected function otherFields()
    {
        return [
            Text::make('Alamat', 'alamat')->hideFromIndex(),
            Text::make('Desa', 'desa')->hideFromIndex(),
            Text::make('Kecamatan', 'kecamatan')->hideFromIndex(),
            Text::make('Kota', 'kota')->hideFromIndex(),
            Text::make('Provinsi', 'provinsi')->hideFromIndex(),
            Text::make('Kode Pos', 'kodepos')->hideFromIndex(),
            Text::make('Nomor Telepon Rumah', 'nomor_telepon_rumah')->hideFromIndex(),
            Text::make('Nomor Telepon Ayah', 'nomor_telepon_ayah')->hideFromIndex(),
            Text::make('Nomor Telepon Ibu', 'nomor_telepon_ibu')->hideFromIndex(),
        ];
    }

    /**
     * Get the biodata fields for the resource.
     *
     * @return array
     */
    protected function fatherFields()
    {
        return [
            Text::make('Nama Ayah', 'name')->hideFromIndex(),
            Select::make('Ayah Masih Hidup?', 'masih_hidup')->options([
                'hidup' => 'Masih Hidup',
                'wafat' => 'Wafat',
            ])->hideFromIndex(),
            Select::make('Pendidikan Ayah', 'pendidikan')->options([
                'sd' => 'SD/MI SEDERAJAD',
                'smp' => 'SMP/MTs SDERAJAD',
                'sma' => 'SMA/MA SEDERAJAD',
                'pt' => 'PERGURUAN TINGGI',
            ])->hideFromIndex(),
            Select::make('Pekerjaan Ayah', 'pekerjaan')->options([
                'buruh_tani' => 'BURUH TANI',
                'tani' => 'TANI',
                'wiraswasta' => 'WIRASWASTA',
                'pns' => 'PNS',
                'tni_polri' => 'TNI/POLRI',
                'lainnya' => 'LAINNYA',
            ])->hideFromIndex(),
            Select::make('Penghasilan Ayah', 'penghasilan')->options([
                '<1.000.000' => '< 1.000.000',
                '1.000.000-2.000.000' => '1.000.000 - 2.000.000',
                '>2.000.000' => '> 2.000.000',
            ])->hideFromIndex(),
        ];
    }

    /**
     * Get the biodata fields for the resource.
     *
     * @return array
     */
    protected function motherFields()
    {
        return [
            Text::make('Nama Ibu', 'ibu_nama')->hideFromIndex(),
            Select::make('Ibu Masih Hidup?', 'ibu_masih_hidup')->options([
                'hidup' => 'Masih Hidup',
                'wafat' => 'Wafat',
            ])->hideFromIndex(),
            Select::make('Pendidikan Ibu', 'ibu_pendidikan')->options([
                'sd' => 'SD/MI SEDERAJAD',
                'smp' => 'SMP/MTs SDERAJAD',
                'sma' => 'SMA/MA SEDERAJAD',
                'pt' => 'PERGURUAN TINGGI',
            ])->hideFromIndex(),
            Select::make('Pekerjaan Ibu', 'ibu_pekerjaan')->options([
                'buruh_tani' => 'BURUH TANI',
                'tani' => 'TANI',
                'wiraswasta' => 'WIRASWASTA',
                'pns' => 'PNS',
                'tni_polri' => 'TNI/POLRI',
                'lainnya' => 'LAINNYA',
            ])->hideFromIndex(),
            Select::make('Penghasilan Ibu', 'ibu_penghasilan')->options([
                '<1.000.000' => '< 1.000.000',
                '1.000.000-2.000.000' => '1.000.000 - 2.000.000',
                '>2.000.000' => '> 2.000.000',
            ])->hideFromIndex(),
        ];
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */    
    public static function indexQuery(NovaRequest $request, $query)
    {
        // filter by user id
        if ( \Auth::user()->pondok_id > 0 )
        {
            $query->where('pondok_id', '=', \Auth::user()->pondok_id);
        }
        
        //const DEFAULT_INDEX_ORDER = 'last_name';
        /*$query->when(empty($request->get('orderBy')), function(Builder $q) {
            $q->getQuery()->orders = [];

            return $q->orderBy(static::DEFAULT_INDEX_ORDER);
        });*/
    }
    
    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        // filter by user id
        if ( \Auth::user()->pondok_id > 0 )
        {
            return [
                new \Sparclex\NovaImportCard\NovaImportCard(Ortu::class),
            ];
        }
        else
        {
            return [];
        }
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel)->withFilename('parents-' . date('dmY-His') . '.xlsx')
                ->withHeadings()
                //->only('merchant_name', 'username'),
                ->allFields()->except('password', 'otp', 'token', 'deleted_at'),
        ];
    }
}
