<?php

namespace App\Console\Commands;

use App\Santri;
use Illuminate\Console\Command;

class SantriLimitReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kazana:reset-limit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Santri Daily Limit Transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $total = 0;
        $santris = Santri::where('transaction_today_total', '>', 0)->get();
        foreach ($santris as $key => $santri) 
        {
            $this->line('Processing '. $santri->name);

            // reset daily limit transaction to 0
            $update = Santri::find($santri->id);
            $update->transaction_today_total = 0;
            $update->save();

            $total++;
        }

        $this->info('Done! '. $total .' santri(s) processed');
    }
}
