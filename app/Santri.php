<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Silvanite\Brandenburg\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class Santri extends Model
{
    use HasRoles;
    use SoftDeletes;

    protected $dates = [
      'deleted_at', 'tanggal_lahir',
    ];

    public function pondok()
    {
        return $this->belongsTo('App\Pondok', 'pondok_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($santri) {
        	if ( !$santri->barcode )
        	{
        		$santri->barcode = $santri->pondok_id .'___'. $santri->nisn .'___'. uniqid();
        	}

            if ( \Auth::check() && \Auth::user()->pondok_id > 0 )
            {
                $santri->pondok_id = \Auth::user()->pondok_id;
            }            

            $santri->transaction_daily_limit = $santri->transaction_daily_limit ?? 0;
            $santri->transaction_today_total = $santri->transaction_today_total ?? 0;
            $santri->no_virtual_account = $santri->no_virtual_account .''. $santri->nisn;

            $santri->status = intval($santri->status) == 1 ? $santri->status : 0;
        });
    }
}
