<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTopupHistory extends Model
{
    use SoftDeletes;

    protected $dates = [
      'deleted_at',
    ];

    public function ortu()
    {
        return $this->belongsTo('App\Ortu', 'ortu_id');
    }

    public function payment_topup()
    {
        return $this->belongsTo('App\PaymentTopup', 'payment_topup_id');
    }

}
