<?php

namespace App;

use App\Kazana\Helper;
use Illuminate\Database\Eloquent\Model;
use Silvanite\Brandenburg\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nominal extends Model
{
    use HasRoles;
    use SoftDeletes;

    protected $dates = [
      'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($nominal) {
        	if ( !$nominal->barcode )
        	{
        		$nominal->barcode = $nominal->value .'___'. Helper::generatePIN() .'___'. uniqid();
        	}
        });
    }
}
