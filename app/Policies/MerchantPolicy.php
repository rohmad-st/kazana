<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class MerchantPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewMerchant', 'manageMerchant'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewMerchant', 'manageMerchant'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageMerchant');
    }

    public function update($user, $post)
    {
        return $user->can('manageMerchant', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageMerchant', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageMerchant', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageMerchant', $post);
    }   
}
