<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class MerchantCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewMerchantCategory', 'manageMerchantCategory'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewMerchantCategory', 'manageMerchantCategory'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageMerchantCategory');
    }

    public function update($user, $post)
    {
        return $user->can('manageMerchantCategory', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageMerchantCategory', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageMerchantCategory', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageMerchantCategory', $post);
    }    
    
}
