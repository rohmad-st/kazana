<?php

namespace App\Policies;

use App\User;
use App\Kazana\Helper;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class SantriPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewSantri', 'manageSantri'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewSantri', 'manageSantri'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageSantri');
    }

    public function update($user, $post)
    {
        return $user->can('manageSantri', $post);
    }

    public function delete($user, $post)
    {
        return Helper::isAdmin() ? $user->can('manageSantri', $post) : false;
    }

    public function restore($user, $post)
    {
        return Helper::isAdmin() ? $user->can('manageSantri', $post) : false;
    }

    public function forceDelete($user, $post)
    {
        return Helper::isAdmin() ? $user->can('manageSantri', $post) : false;
    }   


}
