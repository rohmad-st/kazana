<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class PaymentTopupPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewPaymentTopup', 'managePaymentTopup'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewPaymentTopup', 'managePaymentTopup'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('managePaymentTopup');
    }

    public function update($user, $post)
    {
        return $user->can('managePaymentTopup', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('managePaymentTopup', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('managePaymentTopup', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('managePaymentTopup', $post);
    }   

    
}
