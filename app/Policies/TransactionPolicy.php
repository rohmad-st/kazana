<?php

namespace App\Policies;

//use App\User;
use App\Transaction;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class TransactionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewTransaction', 'manageTransaction'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewTransaction', 'manageTransaction'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageTransaction');
    }

    public function update($user, $post)
    {
        return $user->can('manageTransaction', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageTransaction', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageTransaction', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageTransaction', $post);
    }    
}
