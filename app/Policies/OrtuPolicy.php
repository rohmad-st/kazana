<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class OrtuPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewOrtu', 'manageOrtu'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewOrtu', 'manageOrtu'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageOrtu');
    }

    public function update($user, $post)
    {
        return $user->can('manageOrtu', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageOrtu', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageOrtu', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageOrtu', $post);
    }   

}
