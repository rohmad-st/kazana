<?php

namespace App\Providers;

use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use App\Nova\Metrics\TotalOrtu;
use App\Nova\Metrics\TotalPondok;
use App\Nova\Metrics\TotalSantri;
use Illuminate\Support\Facades\Gate;
use App\Nova\Metrics\TotalMerchants;
use Kazana\QrcodeSantri\QrcodeSantri;
use Kazana\QrSetting\QrSetting;
use Laravel\Nova\NovaApplicationServiceProvider;
use Silvanite\NovaToolPermissions\NovaToolPermissions;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        /*Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                'adi.cahyono@gmail.com'
            ]);
        });*/
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            (new TotalMerchants)->width('1/4'),
            (new TotalOrtu)->width('1/4'),
            (new TotalPondok)->width('1/4'),
            (new TotalSantri)->width('1/4'),
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            new QrcodeSantri,
            new QrSetting,
            new NovaToolPermissions(),
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
