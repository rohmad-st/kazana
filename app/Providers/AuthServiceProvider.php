<?php

namespace App\Providers;


use App\Page;
use App\Ortu;
use App\City;
use App\Santri;
use App\Pondok;
use App\Nominal;
use App\Merchant;
use App\Transaction;
use App\Notification;
use App\PaymentTopup;
use App\MerchantCategory;
use App\Policies\PagePolicy;
use App\Policies\OrtuPolicy;
use App\Policies\CityPolicy;
use App\Policies\SantriPolicy;
use App\Policies\PondokPolicy;
use App\Policies\NominalPolicy;
use App\Policies\MerchantPolicy;
use App\Policies\TransactionPolicy;
use App\Policies\NotificationPolicy;
use Illuminate\Support\Facades\Gate;
use App\Policies\PaymentTopupPolicy;
use App\Policies\MerchantCategoryPolicy;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    use ValidatesPermissions;

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
        Transaction::class => TransactionPolicy::class,
        City::class => CityPolicy::class,
        MerchantCategory::class => MerchantCategoryPolicy::class,
        Merchant::class => MerchantPolicy::class,
        Nominal::class => NominalPolicy::class,
        Ortu::class => OrtuPolicy::class,
        Page::class => PagePolicy::class,
        PaymentTopup::class => PaymentTopupPolicy::class,
        Pondok::class => PondokPolicy::class,
        Santri::class => SantriPolicy::class,
        Notification::class => NotificationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        collect([
            'viewTransaction',
            'manageTransaction',
            'viewCity',
            'manageCity',
            'viewMerchantCategory',
            'manageMerchantCategory',
            'viewMerchant',
            'manageMerchant',
            'viewNominal',
            'manageNominal',
            'viewOrtu',
            'manageOrtu',
            'viewPage',
            'managePage',
            'viewPaymentTopup',
            'managePaymentTopup',
            'viewPondok',
            'managePondok',
            'viewSantri',
            'manageSantri',
            'viewNotification',
            'manageNotification',
        ])->each(function ($permission) {
            Gate::define($permission, function ($user) use ($permission) {
                if ($this->nobodyHasAccess($permission)) {
                    return true;
                }

                return $user->hasRoleWithPermission($permission);
            });
        });

        $this->registerPolicies();

        //
    }
}
