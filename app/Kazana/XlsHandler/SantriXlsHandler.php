<?php
namespace App\Kazana\XlsHandler;

use App\Santri;
use App\Kazana\Helper;
use Sparclex\NovaImportCard\ImportHandler;

class SantriXlsHandler extends ImportHandler
{
    /**
     * Handles the data import
     *
     * @param $resource
     */
    public function handle($resource)
    {
        $data = $this->data;

        if ( !isset($data[0])) return false;

        foreach ($data[0] as $key => $entry) 
        {
        	// skip label
        	if ( $key <= 3 ) continue;

        	// validation, skip if empty : nama, nisn, PIN
        	if ( empty($entry[6]) || empty($entry[22]) || empty($entry[4]) )
        	{
        		continue;
        	}

	        // skip if name is exists
	        $check_name = Santri::where('name', trim($entry[6]))->count();
	        if ( $check_name > 0 )
	        {
	            continue;
	        }

	        // skip if nisn exists
	        $check_nisn = Santri::where('nisn', trim($entry[22]))->count();
	        if ( $check_nisn > 0 )
	        {
	            continue;
	        }

	        // validation success, register Pondok
	        $new_santri = new Santri;
	        $new_santri->description = $entry[1];
	        $new_santri->address = $entry[2];
	        $new_santri->phone = Helper::format_phone($entry[3]);
	        $new_santri->pin = $entry[4];
	        $new_santri->saldo = $entry[5];
	        $new_santri->image = '';

	        $new_santri->name = $entry[6];
	        $new_santri->tempat_lahir = $entry[7];
	        $new_santri->tanggal_lahir = $entry[10] .'-'. $entry[9] .'-'. $entry[8];
	        $new_santri->kelamin = trim($entry[11]) == 'x' ? 1 : 0;
	        $new_santri->anak_ke = $entry[13];
	        $new_santri->jumlah_saudara = $entry[14];
	        $new_santri->golongan_darah = $entry[15];
	        $new_santri->asal_sekolah = $entry[16];
	        $new_santri->asal_sekolah_alamat = $entry[17];
	        $new_santri->asal_sekolah_desa = $entry[18];
	        $new_santri->asal_sekolah_kecamatan = $entry[19];
	        $new_santri->asal_sekolah_kota = $entry[20];
	        $new_santri->asal_sekolah_provinsi = $entry[21];
	        $new_santri->nisn = $entry[22];
	        $new_santri->nomor_un = $entry[23];
	        $new_santri->nomor_skhun = $entry[24];
	        $new_santri->mata_pelajaran = $entry[25];
	        $new_santri->nilai = $entry[26];
	        $new_santri->nama_sekolah = $entry[27];
	        if ($entry[28] == 'x' )
	        {
	        	$new_santri->diterima_dikelas = 'a';
	        }
	        elseif ($entry[29] == 'x' )
	        {
	        	$new_santri->diterima_dikelas = 'b';
	        }
	        elseif ($entry[30] == 'x' )
	        {
	        	$new_santri->diterima_dikelas = 'c';
	        }
	        elseif ($entry[31] == 'x' )
	        {
	        	$new_santri->diterima_dikelas = 'd';
	        }
	        elseif ($entry[32] == 'x' )
	        {
	        	$new_santri->diterima_dikelas = 'e';
	        }
	        elseif ($entry[33] == 'x' )
	        {
	        	$new_santri->diterima_dikelas = 'f';
	        }
	        $new_santri->nama_pondok = $entry[34];
	        $new_santri->nama_kamar = $entry[35];
	        $new_santri->punya_saudara_ponpes = $entry[36] == 'x' ? 1 : 0;
	        $new_santri->jumlah_saudara_ponpes = $entry[38];
	        $new_santri->prestasi_nama = $entry[39];
	        $new_santri->prestasi_tingkat = $entry[40] == 'x' ? 'kota' : $entry[41] == 'x' ? 'provinsi' : $entry[42] == 'x' ? 'nasional' : '';
	        $new_santri->prestasi_tahun = $entry[43];
        	if ($entry[44] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'televisi';
	        }
        	elseif ($entry[45] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'internet';
	        }
        	elseif ($entry[46] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'radio';
	        }
        	elseif ($entry[47] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'majalan';
	        }
        	elseif ($entry[48] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'keluarga';
	        }
        	elseif ($entry[49] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'teman';
	        }
        	elseif ($entry[50] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'tetangga';
	        }
        	elseif ($entry[51] == 'x' )
	        {
		        $new_santri->sumber_informasi = 'lain';
	        }

	        $new_santri->status = 0; // default not active, need super admin apprioal
            if ( \Auth::user()->pondok_id > 0 )
            {
                $new_santri->pondok_id = \Auth::user()->pondok_id;
            }
            else
            {
	        	$new_santri->pondok_id = 1; // default pondok            	
            }

	        $new_santri->save();
        }
    }
}