<?php
namespace App\Kazana\XlsHandler;

use App\Merchant;
use App\Kazana\Helper;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Facades\Hash;
use Sparclex\NovaImportCard\ImportHandler;

class MerchantXlsHandler extends ImportHandler
{
    /**
     * Handles the data import
     *
     * @param $resource
     */
    public function handle($resource)
    {
        $data = $this->data;

        if ( !isset($data[0])) return false;

        foreach ($data[0] as $key => $entry) 
        {
        	// skip label
        	if ( $key <= 1 ) continue;

        	// required, skip if empty : username, password, no registrasi, nama merchant
        	if ( empty($entry[1]) || empty($entry[2]) || empty($entry[7]) || empty($entry[8]) )
        	{
        		continue;
        	}

	        // check if merchant name exists
	        $check_merchant = Merchant::where('merchant_name', $entry[8])->count();
	        if ( $check_merchant >= 1 )
	        {
	            continue;
	        }

	        // check if username exists
	        $check_username = Merchant::where('username', $entry[1])->count();
	        if ( $check_username >= 1 )
	        {
	            continue;
	        }

	        // phone validation
	        $entry[15] = Helper::format_phone($entry[15]);
	        $entry[26] = Helper::format_phone($entry[26]);
	        $entry[35] = Helper::format_phone($entry[35]);

	        // validation success, register merchant
	        $new_merchant = new Merchant;
	        $new_merchant->username = $entry[1] ?? '';
	        $new_merchant->password = Hash::make($entry[2]);
	        $new_merchant->first_name = $entry[3] ?? '';
	        $new_merchant->last_name = $entry[4] ?? '';
	        $new_merchant->description = $entry[5] ?? '';
	        $new_merchant->saldo = $entry[6] ?? 0;
	        $new_merchant->image = '';

	        $new_merchant->no_registrasi = $entry[7] ?? '';
	        $new_merchant->merchant_name = $entry[8] ?? '';
	        $new_merchant->alamat = $entry[10] ?? '';
	        $new_merchant->desa = $entry[11] ?? '';
	        $new_merchant->kecamatan = $entry[12] ?? '';
	        $new_merchant->kota = $entry[13] ?? '';
	        $new_merchant->provinsi = $entry[14] ?? '';
	        $new_merchant->phone = $entry[15] ?? '';   
	        $new_merchant->fax = $entry[16] ?? '';
	        $new_merchant->email = $entry[17] ?? '';
	        $new_merchant->website = $entry[18] ?? '';
	        $new_merchant->pimpinan_nama = $entry[19] ?? '';
	        $new_merchant->pimpinan_alamat = $entry[20] ?? '';
	        $new_merchant->pimpinan_desa = $entry[21] ?? '';
	        $new_merchant->pimpinan_kecamatan = $entry[22] ?? '';
	        $new_merchant->pimpinan_kota = $entry[23] ?? '';
	        $new_merchant->pimpinan_provinsi = $entry[24] ?? '';
	        $new_merchant->pimpinan_kodepos = $entry[25] ?? '';
	        $new_merchant->pimpinan_phone = $entry[26] ?? '';
	        $new_merchant->pimpinan_email = $entry[27] ?? '';
	        $new_merchant->pemilik_nama = $entry[28] ?? '';
	        $new_merchant->pemilik_alamat = $entry[29] ?? '';
	        $new_merchant->pemilik_desa = $entry[30] ?? '';
	        $new_merchant->pemilik_kecamatan = $entry[31] ?? '';
	        $new_merchant->pemilik_kota = $entry[32] ?? '';
	        $new_merchant->pemilik_provinsi = $entry[33] ?? '';
	        $new_merchant->pemilik_kodepos = $entry[34] ?? '';
	        $new_merchant->pemilik_phone = $entry[35] ?? '';
	        $new_merchant->pemilik_email = $entry[36] ?? '';

	        $new_merchant->status = 1; // temporer for testing
	        $new_merchant->merchant_category_id = intval($entry[9]); // default category merchant
            if ( \Auth::user()->pondok_id > 0 )
            {
                $new_merchant->pondok_id = \Auth::user()->pondok_id;
            }
            else
            {
	        	$new_merchant->pondok_id = 1; // default pondok            	
            }	
                    
	        $new_merchant->save();
        }
    }
}