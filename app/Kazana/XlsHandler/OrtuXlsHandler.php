<?php
namespace App\Kazana\XlsHandler;

use App\Ortu;
use App\Santri;
use App\Kazana\Helper;
use Illuminate\Support\Facades\Hash;
use Sparclex\NovaImportCard\ImportHandler;

class OrtuXlsHandler extends ImportHandler
{
    /**
     * Handles the data import
     *
     * @param $resource
     */
    public function handle($resource)
    {
        $data = $this->data;

        if ( !isset($data[0])) return false;

        foreach ($data[0] as $key => $entry) 
        {
        	// skip label
        	if ( $key <= 1 ) continue;

        	// validation, skip if empty : username, password, nisn, nama ayah
        	if ( empty($entry[1]) || empty($entry[2]) || empty($entry[3]) || empty($entry[4]) )
        	{
        		continue;
        	}

	        // check if Santri is exists
	        $check_santri = Santri::where('nisn', $entry[3])->count();
	        if ( $check_santri == 0 )
	        {
	            continue;
	        }

	        // check if username exists
	        $check_username = Ortu::where('username', $entry[1])->count();
	        if ( $check_username >= 1 )
	        {
	            continue;
	        }

	        $row_santri = Santri::where('nisn', $entry[3])->first();

	        // phone validation
	        $entry[42] = Helper::format_phone($entry[42]);
	        $entry[43] = Helper::format_phone($entry[43]);
	        $entry[44] = Helper::format_phone($entry[44]);

	        // validation success, register Ortu
	        $new_ortu = new Ortu;
	        $new_ortu->name = $entry[4];
	        $new_ortu->username = $entry[1];
	        $new_ortu->password = Hash::make($entry[2]);

	        $new_ortu->masih_hidup = $entry[5] == 'x' ? 'hidup' : 'wafat';
	        if ($entry[7] == 'x' )
	        {
		        $new_ortu->pendidikan = 'sd';
	        }
	        elseif ($entry[8] == 'x' )
	        {
		        $new_ortu->pendidikan = 'smp';
	        }
	        elseif ($entry[9] == 'x' )
	        {
		        $new_ortu->pendidikan = 'sma';
	        }
	        elseif ($entry[10] == 'x' )
	        {
		        $new_ortu->pendidikan = 'pt';
	        }

	        if ($entry[11] == 'x' )
	        {
		        $new_ortu->pekerjaan = 'buruh_tani';
	        }
	        elseif ($entry[12] == 'x' )
	        {
		        $new_ortu->pekerjaan = 'tani';
	        }
	        elseif ($entry[13] == 'x' )
	        {
		        $new_ortu->pekerjaan = 'wiraswasta';
	        }
	        elseif ($entry[14] == 'x' )
	        {
		        $new_ortu->pekerjaan = 'pns';
	        }
	        elseif ($entry[15] == 'x' )
	        {
		        $new_ortu->pekerjaan = 'tni_polri';
	        }
	        elseif ($entry[16] == 'x' )
	        {
		        $new_ortu->pekerjaan = 'lainnya';
	        }

	        if ($entry[17] == 'x' )
	        {
		        $new_ortu->penghasilan = '<1.000.000';
	        }
	        elseif ($entry[18] == 'x' )
	        {
		        $new_ortu->penghasilan = '1.000.000-2.000.000';
	        }
	        elseif ($entry[19] == 'x' )
	        {
		        $new_ortu->penghasilan = '>2.000.000';
	        }

	        $new_ortu->ibu_nama = $entry[20];
	        $new_ortu->ibu_masih_hidup = $entry[21] == 'x' ? 'hidup' : 'wafat';
	        if ($entry[23] == 'x' )
	        {
		        $new_ortu->ibu_pendidikan = 'sd';
	        }
	        elseif ($entry[24] == 'x' )
	        {
		        $new_ortu->ibu_pendidikan = 'smp';
	        }
	        elseif ($entry[25] == 'x' )
	        {
		        $new_ortu->ibu_pendidikan = 'sma';
	        }
	        elseif ($entry[26] == 'x' )
	        {
		        $new_ortu->ibu_pendidikan = 'pt';
	        }

	        if ($entry[27] == 'x' )
	        {
		        $new_ortu->ibu_pekerjaan = 'buruh_tani';
	        }
	        elseif ($entry[28] == 'x' )
	        {
		        $new_ortu->ibu_pekerjaan = 'tani';
	        }
	        elseif ($entry[29] == 'x' )
	        {
		        $new_ortu->ibu_pekerjaan = 'wiraswasta';
	        }
	        elseif ($entry[30] == 'x' )
	        {
		        $new_ortu->ibu_pekerjaan = 'pns';
	        }
	        elseif ($entry[31] == 'x' )
	        {
		        $new_ortu->ibu_pekerjaan = 'tni_polri';
	        }
	        elseif ($entry[32] == 'x' )
	        {
		        $new_ortu->ibu_pekerjaan = 'lainnya';
	        }

	        if ($entry[33] == 'x' )
	        {
		        $new_ortu->ibu_penghasilan = '<1.000.000';
	        }
	        elseif ($entry[34] == 'x' )
	        {
		        $new_ortu->ibu_penghasilan = '1.000.000-2.000.000';
	        }
	        elseif ($entry[35] == 'x' )
	        {
		        $new_ortu->ibu_penghasilan = '>2.000.000';
	        }

	        $new_ortu->alamat = $entry[36];
	        $new_ortu->desa = $entry[37];
	        $new_ortu->kecamatan = $entry[38];
	        $new_ortu->kota = $entry[39];
	        $new_ortu->provinsi = $entry[40];
	        $new_ortu->kodepos = $entry[41];
	        $new_ortu->nomor_telepon_rumah = $entry[42];
	        $new_ortu->nomor_telepon_ayah = $entry[43];
	        $new_ortu->nomor_telepon_ibu = $entry[44];

	        $new_ortu->status = 1; // temporer for testing
	        $new_ortu->santri_id = $row_santri->id;
            if ( \Auth::user()->pondok_id > 0 )
            {
                $new_ortu->pondok_id = \Auth::user()->pondok_id;
            }
            else
            {
	        	$new_ortu->pondok_id = 1; // default pondok            	
            }

	        $new_ortu->save();

	        // then get otp, send via sms thirparty
	        $ortu = Ortu::find($new_ortu->id);
	        $otp_number = $ortu->otp;

        }
    }
}