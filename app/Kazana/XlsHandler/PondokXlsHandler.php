<?php
namespace App\Kazana\XlsHandler;

use App\Pondok;
use App\Kazana\Helper;
use Sparclex\NovaImportCard\ImportHandler;

class PondokXlsHandler extends ImportHandler
{
    /**
     * Handles the data import
     *
     * @param $resource
     */
    public function handle($resource)
    {
        $data = $this->data;

        if ( !isset($data[0])) return false;

        foreach ($data[0] as $key => $entry) 
        {
        	// skip label
        	if ( $key <= 1 ) continue;

        	// required, skip if empty : nspp, name
        	if ( empty($entry[2]) || empty($entry[3]) )
        	{
        		continue;
        	}

	        // skip if name is exists
	        $check_name = Pondok::where('name', trim($entry[3]))->count();
	        if ( $check_name > 0 )
	        {
	            continue;
	        }

	        // skip if name is exists
	        $check_nspp = Pondok::where('nspp', trim($entry[2]))->count();
	        if ( $check_nspp > 0 )
	        {
	            continue;
	        }

	        // phone validation
	        $entry[9] = Helper::format_phone($entry[9]);
	        $entry[15] = Helper::format_phone($entry[15]);
	        $entry[19] = Helper::format_phone($entry[19]);

	        // validation success, register Pondok
	        $new_pondok = new Pondok;
	        $new_pondok->image = '';
	        $new_pondok->description = $entry[1] ?? '';
	      	$new_pondok->nspp = $entry[2] ?? '';
	        $new_pondok->name = $entry[3] ?? '';
	        $new_pondok->address = $entry[4] ?? '';
	      	$new_pondok->desa = $entry[5] ?? '';
	      	$new_pondok->kecamatan = $entry[6] ?? '';
	      	$new_pondok->kota = $entry[7] ?? '';
	      	$new_pondok->provinsi = $entry[8] ?? '';
	        $new_pondok->phone = $entry[9] ?? '';
	      	$new_pondok->fax = $entry[10] ?? '';
	      	$new_pondok->email = $entry[11] ?? '';
	      	$new_pondok->website = $entry[12] ?? '';
	      	$new_pondok->ponpes_kepala = $entry[13] ?? '';
	      	$new_pondok->ponpes_alamat = $entry[14] ?? '';
	      	$new_pondok->ponpes_telepon = $entry[15] ?? '';
	      	$new_pondok->ponpes_email = $entry[16] ?? '';
	      	$new_pondok->yayasan_nama = $entry[17] ?? '';
	      	$new_pondok->yayasan_alamat = $entry[18] ?? '';
	      	$new_pondok->yayasan_telepon = $entry[19] ?? '';
	      	$new_pondok->yayasan_email = $entry[20] ?? '';

	        $new_pondok->city_id = 1; // default
	        $new_pondok->status = 1;
	        $new_pondok->save();
        }
    }
}