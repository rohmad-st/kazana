<?php
namespace App\Kazana;

class Helper
{
    public function __construct() 
    {
    }

    /**
     * Normalize phone number format
     */
    public static function format_phone($phone)
    {
        // allow numeric only
        $phone = preg_replace('/[^0-9]/', '', $phone);

        // invalid phone number
        if ( strlen($phone) <= 5 ) return '';

        // remove leading zero
        if (preg_match('/^0/', $phone))
        {
            $phone = preg_replace('/^0/', '', $phone);
        }

        // if not start with 62, then add 62
        if (!preg_match('/^62/', $phone))
        {
            $phone = '62'. $phone;
        }

        // bug fix, temporary
        $phone = preg_replace('/^6262/', '62', $phone);

        return $phone;
    }

    /**
     * Assets image cdn generator url
     */
    public static function image_url($width, $height, $image)
    {
        return config('app.cdn') .'/'. $width .'/'. $height . \Storage::url($image);
    }

    /**
     * Cleanup string such as removing html, punctuation
     * @param  string
     * @return array
     */
    private static function stringCleanup($string)
    {
        // strip all html tags
        $string = strip_tags($string);
        // convert to lowercase
        $string = mb_strtolower($string);
        // strip punctuation
        $string = preg_replace('/[^a-z]+/i', ' ', $string);
        // split words
        $array = preg_split('/\s+/', $string);
        return $array;
    }

    public static function generatePIN($digits = 6){
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public static function isAdmin()
    {
        if ( preg_match('/(admin|root)\@kazana\.id$/i', \Auth::user()->email ))
        {
            return true;
        }

        return false;
    }
}