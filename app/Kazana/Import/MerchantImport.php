<?php
namespace App\Kazana\Import;

use App\Merchant;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class MerchantImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        if (!isset($row[0])) 
        {
            return null;
        }

        return $row;
        return new Merchant([
           'merchant_name' => $row[0],
           'username' => $row[1], 
           'password' => $row[2],
           'first_name' =>  $row[3], 
           'last_name' =>  $row[4], 
           'description' =>  $row[5], 
           'phone' =>  $row[6],
           'saldo' =>  $row[7],
        ]);
    }
}