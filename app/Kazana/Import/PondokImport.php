<?php
namespace App\Kazana\Import;

use App\Pondok;
use Maatwebsite\Excel\Concerns\ToModel;

class PondokImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        if (!isset($row[0])) 
        {
            return null;
        }

        return $row;
    }
}