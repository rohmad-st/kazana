<?php
namespace App\Kazana\Import;

use App\Santri;
use Maatwebsite\Excel\Concerns\ToModel;

class SantriImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        if (!isset($row[0])) 
        {
            return null;
        }

        return $row;
    }
}