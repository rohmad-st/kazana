<?php
namespace App\Kazana\XlsReader;

use App\Kazana\Import\OrtuImport;
use Maatwebsite\Excel\Facades\Excel;
use Sparclex\NovaImportCard\ImportFileReader;

class OrtuXlsReader extends ImportFileReader 
{
    public static function mimes()
    {
        return 'xls,xlsx';
    }

    public function read(): array
    {
    	$original_file_name = $this->file->getClientOriginalName();
        copy($this->file->getRealPath(), storage_path('app/ortu_'. $original_file_name));
        $data = Excel::toArray(new OrtuImport, 'ortu_'. $original_file_name);

        return $data;
    }    
}