<?php
namespace App\Kazana\XlsReader;

use App\Kazana\Import\SantriImport;
use Maatwebsite\Excel\Facades\Excel;
use Sparclex\NovaImportCard\ImportFileReader;

class SantriXlsReader extends ImportFileReader 
{
    public static function mimes()
    {
        return 'xls,xlsx';
    }

    public function read(): array
    {
    	$original_file_name = $this->file->getClientOriginalName();
        copy($this->file->getRealPath(), storage_path('app/santri_'. $original_file_name));
        $data = Excel::toArray(new SantriImport, 'santri_'. $original_file_name);

        return $data;
    }    
}