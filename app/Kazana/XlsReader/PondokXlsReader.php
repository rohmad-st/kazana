<?php
namespace App\Kazana\XlsReader;

use App\Kazana\Import\PondokImport;
use Maatwebsite\Excel\Facades\Excel;
use Sparclex\NovaImportCard\ImportFileReader;

class PondokXlsReader extends ImportFileReader 
{
    public static function mimes()
    {
        return 'xls,xlsx';
    }

    public function read(): array
    {
    	$original_file_name = $this->file->getClientOriginalName();
        copy($this->file->getRealPath(), storage_path('app/pondok_'. $original_file_name));
        $data = Excel::toArray(new PondokImport, 'pondok_'. $original_file_name);

        return $data;
    }    
}