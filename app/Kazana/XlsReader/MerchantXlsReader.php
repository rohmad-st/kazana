<?php
namespace App\Kazana\XlsReader;

use App\Kazana\Import\MerchantImport;
use Laravel\Nova\Actions\Action;
use Maatwebsite\Excel\Facades\Excel;
use Sparclex\NovaImportCard\ImportFileReader;

class MerchantXlsReader extends ImportFileReader 
{
    public static function mimes()
    {
        return 'xls,xlsx';
    }

    public function read(): array
    {
    	$original_file_name = $this->file->getClientOriginalName();
        copy($this->file->getRealPath(), storage_path('app/merchant_'. $original_file_name));
        $data = Excel::toArray(new MerchantImport, 'merchant_'. $original_file_name);

        return $data;
    }    
}