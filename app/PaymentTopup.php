<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Silvanite\Brandenburg\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTopup extends Model
{
    use HasRoles;
    use SoftDeletes;

    protected $dates = [
      'deleted_at',
    ];
}
