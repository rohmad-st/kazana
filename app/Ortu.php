<?php

namespace App;

use App\Kazana\Helper;
use Illuminate\Database\Eloquent\Model;
use Silvanite\Brandenburg\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ortu extends Model
{
    use HasRoles;
    use SoftDeletes;

    protected $dates = [
      'deleted_at',
    ];

    public function pondok()
    {
        return $this->belongsTo('App\Pondok', 'pondok_id', 'id');
    }

    public function santri()
    {
        return $this->belongsTo('App\Santri', 'santri_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($ortu) {
        	if ( !$ortu->otp )
        	{
        		$ortu->otp = Helper::generatePIN();
        	}

            if ( \Auth::check() && \Auth::user()->pondok_id > 0 )
            {
                $ortu->pondok_id = \Auth::user()->pondok_id;
            }  
        });
    }
}
