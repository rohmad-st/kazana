<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Ortu;
use App\Pondok;
use App\Santri;
use App\Kazana\SMS;
use App\Transaction;
use App\PaymentTopup;
use App\Kazana\Helper;
use Illuminate\Support\Str;
use App\PaymentTopupHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\PaymentTopupHistoryCollection;

class OrtuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5|max:30',
            'phone' => 'required|numeric',
            'username' => 'required|min:5|max:30',
            'password' => 'required|min:5',
            'nisn' => 'required|numeric',
            'pondok_id' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if Santri is exists
        $check_santri = Santri::where('nisn', $request->input('nisn'))->count();
        if ( $check_santri == 0 )
        {
            return response()->json(['rd'=>'NISN not exists', 'rc' => '500', 'data' => ['NISN not exists']], 200);
        }

        // check if Santri not yet registered
        $check_santri_row = Santri::where('nisn', $request->input('nisn'))->first();
        $check_santri_ortu = Ortu::where('santri_id', $check_santri_row->id)->count();
        if ( $check_santri_ortu > 0 )
        {
            return response()->json(['rd'=>'Santri already registered', 'rc' => '500', 'data' => ['Santri already registered']], 200);
        }

        // check if username exists
        $check_username = Ortu::where('username', $request->input('username'))->count();
        if ( $check_username >= 1 )
        {
            return response()->json(['rd'=>'Username already exists', 'rc' => '500', 'data' => ['Username already exists']], 200);
        }

        // check if pondok exists
        $check_pondok_id = Pondok::where('id', $request->input('pondok_id'))->count();
        if ( $check_pondok_id == 0 )
        {
            return response()->json(['rd'=>'Pondok not exists', 'rc' => '500', 'data' => ['Pondok not exists']], 200);
        }

        $row_santri = Santri::where('nisn', $request->input('nisn'))->first();

        // validation success, register Ortu
        $new_ortu = new Ortu;
        $new_ortu->name = $request->input('name');
        $new_ortu->phone = Helper::format_phone($request->input('phone'));
        $new_ortu->username = $request->input('username');
        $new_ortu->password = Hash::make($request->input('password'));
        $new_ortu->status = 0; // temporer for testing
        $new_ortu->santri_id = $row_santri->id;
        $new_ortu->pondok_id = $request->input('pondok_id');
        $new_ortu->save();

        // then get otp, send via sms thirparty
        $ortu = Ortu::find($new_ortu->id);
        //$data['otp_number'] = $ortu->otp;
        $data['message'] = 'Pendaftaran berhasil, silahkan verifikasi akun Anda';
        $to_phone_number = Helper::format_phone($request->input('phone'));
        if ( $to_phone_number )
        {
            SMS::send($to_phone_number, 'Kazana.id - Kode verifikasi Anda adalah '. $ortu->otp);
        }

        return response()->json(['rd'=>'Pendaftaran berhasil, silahkan verifikasi akun Anda', 'rc' => '200', 'data' => $data], 200);
    }

    public function resend_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'Semua isian harus di isi', 'rc' => '500', 'data' => $data], 200);
        }

        $merchant_otp = Ortu::where('phone', Helper::format_phone($request->input('phone')))->first();

        if ( !$merchant_otp )
        {
            return response()->json(['rd'=>'Nomor telepon tidak terdaftar', 'rc' => '500', 'data' => ['message' => '']], 200);
        }

        if ( $merchant_otp->status == 1 )
        {
            return response()->json(['rd'=>'Nomor telepon sudah ter-verifikasi', 'rc' => '500', 'data' => ['message' => '']], 200);
        }

        // then get otp, send via sms thirparty
        $code_otp = $merchant_otp->otp;
        $to_phone_number = Helper::format_phone($request->input('phone'));
        if ( $to_phone_number )
        {
            SMS::send($to_phone_number, 'Kazana.id - Kode verifikasi Anda adalah '. $code_otp);
        }

        return response()->json(['rd'=>'Pengiriman ulang kode OTP sukses.', 'rc' => '200', 'data' => ['message' => 'Pengiriman ulang kode OTP sukses']], 200);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\Ortu  $ortu
     * @return \Illuminate\Http\Response
     */
    public function edit(Ortu $ortu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\Ortu  $ortu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ortu $ortu)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();

        if ( !$ortu )
        {
            return response()->json(['rd'=>'user tidak ditemukan', 'rc' => '500', 'data' => ['data' => 'user tidak ditemukan'] ], 200);
        }

        $ortu_id = $ortu->id;
        $pondok_id = $ortu->pondok_id;

        $validator = Validator::make($request->all(), [
            'username' => 'required|min:5|max:30',
            'nama_ayah' => 'required|min:3|max:30',
            'nisn' => 'required',
            'phone' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if Santri is exists
        $check_santri = Santri::where('nisn', $request->input('nisn'))->count();
        if ( $check_santri == 0 )
        {
            return response()->json(['rd'=>'NISN Santri tidak ditemukan', 'rc' => '500', 'data' => ['NISN Santri tidak ditemukan']], 200);
        }

        // check if username exists
        /*$check_username = Ortu::where('username', $request->input('username'))->count();
        if ( $check_username >= 1 )
        {
            return response()->json(['rd'=>'Username sudah ada yang menggunakan', 'rc' => '500', 'data' => ['Username sudah ada yang menggunakan']], 200);
        }*/

        //$row_santri = Santri::where('nisn', $request->input('nisn'))->first();

        if ( $request->input('image') && strpos($request->input('image'), 'http://kazana.dev-support.net') === false ) //&& file_exists(storage_path('tmp/'. $request->input('image'))))
        {
            @Storage::move('tmp/'. $request->input('image'), 'public/'. $request->input('image'));
            $new_image = $request->input('image');
        }
        else if (strpos($request->input('image'), 'http://kazana.dev-support.net') !== false && basename($request->input('image')) != 'user-dummy.png')
        {
            $new_image = basename($request->input('image'));
        }
        else
        {
            $new_image = '';
        }

        // validation success, update profile Ortu
        $ortu = Ortu::where('id', $ortu_id)->first();
        $ortu->phone = Helper::format_phone($request->input('phone'));
        $ortu->alamat = $request->input('alamat');
        $ortu->desa = $request->input('desa');
        $ortu->kecamatan = $request->input('kecamatan');
        $ortu->kota = $request->input('kota');
        $ortu->provinsi = $request->input('provinsi');
        $ortu->kodepos = $request->input('kodepos');
        $ortu->nomor_telepon_rumah = $request->input('nomor_telepon_rumah');
        $ortu->nomor_telepon_ayah = $request->input('nomor_telepon_ayah');
        $ortu->nomor_telepon_ibu = $request->input('nomor_telepon_ibu');

        $ortu->name = $request->input('nama_ayah');
        $ortu->masih_hidup = $request->input('masih_hidup');
        $ortu->pendidikan = $request->input('pendidikan');
        $ortu->pekerjaan = $request->input('pekerjaan');
        $ortu->penghasilan = $request->input('penghasilan');
        $ortu->image = $new_image;

        $ortu->ibu_nama = $request->input('ibu_nama');
        $ortu->ibu_pendidikan = $request->input('ibu_pendidikan');
        $ortu->ibu_pekerjaan = $request->input('ibu_pekerjaan');
        $ortu->ibu_penghasilan = $request->input('ibu_penghasilan');
        $ortu->ibu_masih_hidup = $request->input('ibu_masih_hidup');

        $ortu->save();

        return $this->show(new Ortu(), $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Ortu $ortu, Request $request)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::with(['pondok', 'santri'])->where('token', $user_token)->where('status', 1)->first();

        if ( $ortu == null )
        {
            return response()->json(['rd'=>'Data tidak ditemukan', 'rc' => '500', 'data' => ['Data tidak ditemukan']], 200);
        }

        $ortu_id = $ortu->id;
        $pondok_id = $ortu->pondok_id;

        $data['nama_ayah'] = $ortu->name;
        $data['phone'] = Helper::format_phone($ortu->phone) ?? '';
        $data['username'] = $ortu->username;
        $data['masih_hidup'] = $ortu->masih_hidup ?? '';
        $data['pendidikan'] = $ortu->pendidikan ?? '';

        $data['pekerjaan'] = $ortu->pekerjaan ?? '';
        $data['penghasilan'] = $ortu->penghasilan ?? '';
        $data['ibu_nama'] = $ortu->ibu_nama ?? '';
        $data['ibu_masih_hidup'] = $ortu->ibu_masih_hidup ?? '';
        $data['ibu_pendidikan'] = $ortu->ibu_pendidikan ?? '';

        $data['ibu_pekerjaan'] = $ortu->ibu_pekerjaan ?? '';
        $data['ibu_penghasilan'] = $ortu->ibu_penghasilan ?? '';
        $data['alamat'] = $ortu->alamat ?? '';

        $data['desa'] = $ortu->desa;
        $data['image'] = $ortu->image ? Helper::image_url(600, 600, $ortu->image) : 'http://kazana.dev-support.net/custom/img/user-dummy.png';
        $data['nisn'] = $ortu->santri->nisn;

        $data['kecamatan'] = $ortu->kecamatan ?? '';
        $data['kota'] = $ortu->kota ?? '';
        $data['provinsi'] = $ortu->provinsi ?? '';
        $data['kodepos'] = $ortu->kodepos ?? '';
        $data['nomor_telepon_rumah'] = $ortu->nomor_telepon_rumah ?? '';
        $data['nomor_telepon_ayah'] = $ortu->nomor_telepon_ayah ?? '';
        $data['nomor_telepon_ibu'] = $ortu->nomor_telepon_ibu ?? '';

        $data['created_at'] = date('d F Y', strtotime($ortu->created_at));

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\Ortu  $ortu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ortu $ortu)
    {
        //
    }

    public function logout(Request $request)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();

        if ( $ortu == null )
        {
            return response()->json(['rd'=>'error', 'rc' => '200', 'data' => ['Ortu not exists']], 200);
        }

        $ortu->token = '';
        $ortu->save();

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => []], 200);        
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:5|max:30',
            'password' => 'required|min:5',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if Ortu name exists
        $check_ortu = Ortu::where('username', $request->input('username'));
        if ( $check_ortu->count() == 0 )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Username tidak terdaftar']], 200);
        }

        $ortu = $check_ortu->first();

        if ( $ortu->status == 0 )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Akun Anda belum ter-verifikasi']], 200);
        }

        if (Hash::check($request->input('password'), $ortu->password))
        {
            $data['token'] = $ortu->id . Str::random(32);
            $ortu->token = $data['token'];
            $ortu->save();
        }
        else
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Password tidak benar']], 200);
        }

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);        
    }

    public function otp_confirm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required|min:6',
            'username' => 'required|min:5|max:30',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if otp exists
        $check_Ortu = Ortu::where('otp', $request->input('otp'))->where('username', $request->input('username'));
        if ( $check_Ortu->count() == 0 )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Code not exists']], 200);
        }

        $ortu = $check_Ortu->first();

        if ( $ortu->status == 1 )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Your account already activated']], 200);
        }

        $ortu->status = 1;
        $ortu->save();

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => ['message' => 'Verifikasi berhasil']], 200);
    }

    public function dashboard(Request $request)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();
        $ortu_id = $ortu->id;
        $pondok_id = $ortu->pondok_id;
        $santri_id = $ortu->santri_id;

        $current = Transaction::where('status', 1)->where('santri_id', $santri_id);
        $this_month = clone($current);

        $santri = Santri::where('id', $santri_id)->first();

        $data['saldo'] = $santri->saldo ?? 0;
        $data['last'] = 0;
        $data['this_month'] = 0;

        $current = $current->orderBy('created_at', 'DESC')->take(1)->first();
        $data['last'] = $current->amount ?? 0;

        $this_month = $this_month->selectRaw('SUM(amount) as total')->whereRaw('YEAR(created_at)=YEAR(NOW()) AND MONTH(created_at)=MONTH(NOW())')->first();
        $data['this_month'] = intval($this_month->total ?? 0);

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);
    }


    public function password_update(Request $request)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();
        $ortu_id = $ortu->id;

        $validator = Validator::make($request->all(), [
            'password_old' => 'required|min:5',
            'password_new' => 'required|min:5',
            'password_new2' => 'required|min:5',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        if ( $request->input('password_new') != $request->input('password_new2') )
        {
            return response()->json(['rd'=>'Password baru tidak cocok', 'rc' => '500', 'data' => ['message' => 'Password baru tidak cocok']], 200);
        }

        if (Hash::check($request->input('password_old'), $ortu->password))
        {
            $new_ortu = Ortu::find($ortu->id);
            $new_ortu->password = Hash::make($request->input('password_new'));
            $new_ortu->save();

            return response()->json(['rd'=>'Password sukses diubah', 'rc' => '200', 'data' => ['message' => 'Password sukses diubah']], 200);
        }
        else
        {
            return response()->json(['rd'=>'Password salah', 'rc' => '500', 'data' => ['Password salah']], 200);
        }

    }

    public function payment_history(Request $request)
    {
        $by_year = intval($request->input('year'));
        $by_month = intval($request->input('month'));
        $by_date = intval($request->input('day'));

        $payment_topup_history = PaymentTopupHistory::with(['payment_topup', 'ortu']);

        if ( $by_year && $by_month && $by_date )
        {
            $payment_topup_history->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\' AND DAY(created_at)=\''. $by_date .'\'');
        }
        else if ( $by_year && $by_month )
        {
            $payment_topup_history->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\'');
        }

        $rows_total = clone ($payment_topup_history);
        $total_amount = intval($rows_total->sum('amount'));

        $payment_topup_history = $payment_topup_history->orderByDesc('created_at')->paginate(5)->appends(['month' => $by_month, 'day' => $by_date, 'year' => $by_year]);

        return (new PaymentTopupHistoryCollection($payment_topup_history))->additional(['data' => ['total_amount' => $total_amount]]);
    }
}