<?php

namespace App\Http\Controllers\Api;

use App\PaymentTopup;
use App\PaymentTopupHistory;
use App\Ortu;
use App\Santri;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PaymentTopupCollection;

class PaymentTopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $nisn)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5|max:30',
            'description' => 'required|min:5|max:250',
            'amount' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails())
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message)
            {
                $data[$key] = $message;
            }

            return response()->json(['status' => false, 'message' => $data, 'data' => null], 400);
        }

        // check if Santri not yet registered
        $check_santri_row = Santri::where('nisn', $nisn)->first();
        if (!isset($check_santri_row))
        {
            return response()->json(['status' => false, 'message' => 'Santri tidak ditemukan', 'data' => null], 404);
        }

        $check_santri_ortu = Ortu::where('santri_id', $check_santri_row['id'])->first();

        // Save to table payment_topup
        $payment = new PaymentTopup();
        $payment->name = $request->input('name');
        $payment->description = $request->input('description');
        $payment->image = '';
        $payment->status = 1;
        $payment->save();

        // Save to table payment_topup_history
        $paymentHistory = new PaymentTopupHistory();
        $paymentHistory->amount = $request->input('amount');
        $paymentHistory->ortu_id = isset($check_santri_ortu) ? $check_santri_ortu['id'] : 0;
        $paymentHistory->payment_topup_id = isset($payment) ? $payment['id'] : 0;
        $paymentHistory->save();
        // $paymentHistory->payment_topup()->save($payment);
        // $paymentHistory->ortu()->save($check_santri_ortu);

        $result = [
            "billInfo1" => $payment->name,
            "billInfo2" => null,
            "billInfo3" => null,
            "billInfo4" => null,
            "billInfo5" => "TOPUP",
            "billInfo6" => 2019,
            "billInfo7" => "Semester Genap",
            "billInfo8" => "Reguler"
        ];

        return response()->json(['status' => true, 'message' => 'Berhasil data topup ditambahkan', 'data' => $result], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\PaymentTopup  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentTopup $payment)
    {
        return new PaymentTopupCollection($payment->orderBy('name')->paginate(5));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\PaymentTopup  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentTopup $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\PaymentTopup  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentTopup $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\PaymentTopup  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentTopup $payment)
    {
        //
    }
}
