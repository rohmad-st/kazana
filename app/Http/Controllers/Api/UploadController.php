<?php 

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
  public function __construct() 
  {
    
  }
  
  public function endpoint(Request $request) 
  {
      if($request->hasFile('file'))
      {
          $file = $request->file('file');
          $filename = str_random(20) . time() .'.'. $file->getClientOriginalExtension();
          Storage::putFileAs('tmp', $file, $filename);

          return $filename;
      }
    
      return 'false';
  }
}
/* eof */