<?php

namespace App\Http\Controllers\Api;

use App\Ortu;
use App\Santri;
use App\Pondok;
use App\Merchant;
use App\Transaction;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\TransactionCollection;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    public function order(Request $request)
    {
        $amount = intval($request->input('amount'));
        $note = $request->input('note');
        $santri_id = intval($request->input('santri_id'));

        if ( $amount == 0 )
        {
            return response()->json(['rd'=>'Nominal transaksi harus lebih dari 0', 'rc' => '500', 'data' => ['Nominal transaksi harus lebih dari 0']], 200);
        }

        if ( $santri_id == 0 )
        {
            return response()->json(['rd'=>'Santri tidak terdaftar', 'rc' => '500', 'data' => ['Santri tidak terdaftar']], 200);
        }

        // check saldo
        $santri = Santri::where('id', $santri_id)->first();

        if ( $santri == null) 
        {
            return response()->json(['rd'=>'Santri tidak terdaftar', 'rc' => '500', 'data' => ['Santri tidak terdaftar']], 200);
        }

        if ( $amount > $santri->saldo )
        {
            return response()->json(['rd'=>'Saldo tidak mencukupi', 'rc' => '500', 'data' => ['Saldo tidak mencukupi']], 200);
        }

        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();
        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        // save
        $transaction = new Transaction;
        $transaction->note = $note;
        $transaction->invoice = '';
        $transaction->amount = $amount;
        $transaction->status = 0;
        $transaction->santri_id = $santri_id;
        $transaction->merchant_id = $merchant_id;
        $transaction->pondok_id = $pondok_id;
        $transaction->save();

        // update invoice id
        $update_transaction = Transaction::find($transaction->id);
        $invoice_id = $pondok_id .'P'. $merchant_id .'M'. date('ymd') .'D'. $transaction->id;
        $update_transaction->invoice = $invoice_id;
        $update_transaction->save();

        // reduce saldo
        /*
        $santri = Santri::where('id', $santri_id)->first();
        $remaining_saldo = $santri->saldo - $amount;
        $santri->saldo = $remaining_saldo;
        $santri->save();
        */

        // invoice data
        $data['id'] = $transaction->id;
        $data['name'] = $santri->name;
        $data['nisn'] = $santri->nisn;
        $data['invoice'] = $invoice_id;
        $data['amount'] = $amount;
        //$data['saldo'] = $remaining_saldo;
        $data['date'] = date('Y-m-d H:i:s', strtotime($update_transaction->created_at));

        return response()->json(['rd'=>'Pembayaran berhasil dicatat', 'rc' => '200', 'data' => $data], 200);
    }

    public function confirm(Request $request)
    {
        $santri_id = intval($request->input('santri_id'));
        $invoice = $request->input('invoice');
        $pin = $request->input('pin');

        // check transaction detail
        $transaction = Transaction::where('invoice', $invoice)->first();

        if ( $transaction == null) 
        {
            return response()->json(['rd'=>'Invoice tidak terdaftar', 'rc' => '500', 'data' => ['Invoice tidak terdaftar']], 200);
        }

        // check saldo
        $santri = Santri::where('id', $santri_id)->first();

        if ( $santri == null) 
        {
            return response()->json(['rd'=>'Santri tidak terdaftar', 'rc' => '500', 'data' => ['Santri tidak terdaftar']], 200);
        }

        if (!Hash::check($request->input('pin'), $santri->pin))
        {
            return response()->json(['rd'=>'PIN salah', 'rc' => '500', 'data' => ['PIN salah']], 200);
        }

        if ( $transaction->status == 1 )
        {
            return response()->json(['rd'=>'Transaksi sudah dibayar', 'rc' => '500', 'data' => ['Transaksi sudah dibayar']], 200);
        }

        if ( $transaction->amount > $santri->saldo )
        {
            return response()->json(['rd'=>'Saldo tidak mencukupi', 'rc' => '500', 'data' => ['Saldo tidak mencukupi']], 200);
        }

        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();
        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        // reduce saldo santri
        // AND update daily transaction santri
        $remaining_saldo = $santri->saldo - $transaction->amount;
        $santri->saldo = $remaining_saldo;
        $santri->transaction_today_total = $santri->transaction_today_total + $transaction->amount;
        $santri->save();

        // update status transaction
        $transaction->saldo_balance = $remaining_saldo;
        $transaction->status = 1;
        $transaction->save();

        // update saldo merchant
        $merchant->saldo = $merchant->saldo + $transaction->amount;
        $merchant->save();

        // update notification
        $notification = new Notification;
        $notification->message = 'Transaksi ID '. $invoice .' a/n '. $santri->name .' berhasil!';
        $notification->merchant_id = $merchant_id;
        $notification->santri_id = $santri_id;
        $notification->pondok_id = $pondok_id;
        $notification->save();

        // invoice data
        $data['id'] = $transaction->id;
        $data['name'] = $santri->name;
        $data['nisn'] = $santri->nisn;
        $data['invoice'] = $invoice;
        $data['amount'] = $transaction->amount;
        $data['saldo'] = $remaining_saldo;
        $data['date'] = date('Y-m-d H:i:s', strtotime($transaction->created_at));

        return response()->json(['rd'=>'Pembayaran berhasil!', 'rc' => '200', 'data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    public function history(Request $request)
    {
        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();
        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        $by_year = intval($request->input('year'));
        $by_month = intval($request->input('month'));
        $by_date = intval($request->input('day'));

        $rows = Transaction::with(['santri', 'pondok'])->where('status', 1)->where('merchant_id', $merchant_id);

        if ( $by_year && $by_month && $by_date )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\' AND DAY(created_at)=\''. $by_date .'\'');
        }
        else if ( $by_year && $by_month )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\'');
        }

        $rows_total = clone ($rows);
        $total_amount = intval($rows_total->sum('amount'));

        //return new TransactionCollection($rows->orderBy('created_at', 'DESC')->get());
        return (new TransactionCollection($rows->orderBy('created_at', 'DESC')->paginate(5)->appends(['month' => $by_month, 'day' => $by_date, 'year' => $by_year])))
        	->additional(['data' => ['total_amount' => $total_amount]]);
    }

    public function ortu_history(Request $request)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();
        $ortu_id = $ortu->id;
        $pondok_id = $ortu->pondok_id;
        $santri_id = $ortu->santri_id;

        $by_year = intval($request->input('year'));
        $by_month = intval($request->input('month'));
        $by_date = intval($request->input('day'));

        $rows = Transaction::with(['santri', 'pondok'])->where('status', 1)->where('santri_id', $santri_id);

        if ( $by_year && $by_month && $by_date )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\' AND DAY(created_at)=\''. $by_date .'\'');
        }
        else if ( $by_year && $by_month )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\'');
        }

        $rows_total = clone ($rows);
        $total_amount = intval($rows_total->sum('amount'));

        //return new TransactionCollection($rows->orderBy('created_at', 'DESC')->get());
        return (new TransactionCollection($rows->orderBy('created_at', 'DESC')->paginate(5)->appends(['month' => $by_month, 'day' => $by_date, 'year' => $by_year])))
            ->additional(['data' => ['total_amount' => $total_amount]]);
    }
}
