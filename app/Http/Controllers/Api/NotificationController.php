<?php

namespace App\Http\Controllers\Api;

use App\Ortu;
use App\Santri;
use App\Pondok;
use App\Merchant;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\NotificationCollection;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }

    public function history(Request $request)
    {
        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();
        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        $by_year = intval($request->input('year'));
        $by_month = intval($request->input('month'));
        $by_date = intval($request->input('day'));

        $rows = Notification::with(['santri', 'pondok'])->where('merchant_id', $merchant_id);

        if ( $by_year && $by_month && $by_date )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\' AND DAY(created_at)=\''. $by_date .'\'');
        }
        else if ( $by_year && $by_month )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\'');
        }

        return new NotificationCollection($rows->orderBy('created_at', 'DESC')->paginate(5)->appends(['month' => $by_month, 'day' => $by_date, 'year' => $by_year]));
    }

    public function update_status_history(Request $request)
    {
        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();
        $id = intval($request->input('id'));

        if ( $id == 0 || $merchant == null )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['message' => 'false']], 200);
        }

        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        $rows = Notification::where('merchant_id', $merchant_id)->where('id', $id)->take(1)->update(['status' => 1]);

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => ['message' => 'true']], 200);
    }

    public function ortu_history(Request $request)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();
        $ortu_id = $ortu->id;
        $pondok_id = $ortu->pondok_id;
        $santri_id = $ortu->santri_id;

        $by_year = intval($request->input('year'));
        $by_month = intval($request->input('month'));
        $by_date = intval($request->input('day'));

        $rows = Notification::with(['santri', 'pondok'])->where('santri_id', $santri_id);

        if ( $by_year && $by_month && $by_date )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\' AND DAY(created_at)=\''. $by_date .'\'');
        }
        else if ( $by_year && $by_month )
        {
            $rows->whereRaw('YEAR(created_at)=\''. $by_year .'\' AND MONTH(created_at)=\''. $by_month .'\'');
        }

        return new NotificationCollection($rows->orderBy('created_at', 'DESC')->paginate(5)->appends(['month' => $by_month, 'day' => $by_date, 'year' => $by_year]));
    }

    public function update_status_ortu_history(Request $request)
    {
        $user_token = $request->header('token');
        $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();
        $id = intval($request->input('id'));

        if ( $id == 0 || $ortu == null )
        {
            return response()->json(['rd'=>'Gagal mengubah data notifikasi', 'rc' => '500', 'data' => ['message' => 'false']], 200);
        }

        $santri_id = $ortu->santri_id;

        $rows = Notification::where('santri_id', $santri_id)->where('id', $id)->take(1)->update(['status' => 1]);

        return response()->json(['rd'=>'Notifikasi berhasil diubah', 'rc' => '200', 'data' => ['message' => 'true']], 200);        
    }
}
