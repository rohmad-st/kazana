<?php

namespace App\Http\Controllers\Api;

use App\Santri;
use App\Nominal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SantriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function show(Santri $santri)
    {
    }

    public function scan_santri(Request $request)
    {
        //dd($request->all());
        $row = Santri::with(['pondok'])->where('barcode', $request->input('bar'))->first();
        if ( $row == null )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Santri not Exists']], 200);
        }

        $data['id'] = $row->id;
        $data['name'] = $row->name;
        $data['pondok'] = $row->pondok->name;
        $data['saldo'] = $row->saldo;
        $data['nisn'] = $row->nisn;

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);
    }

    public function scan_nominal(Request $request)
    {
        $row = Nominal::where('barcode', $request->input('bar'))->first();
        if ( $row == null )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Barcode not Exists']], 200);
        }

        $data['nominal'] = $row->value;

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function edit(Santri $santri)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Santri $santri)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function destroy(Santri $santri)
    {
        //
    }

}
