<?php

namespace App\Http\Controllers\Api;

use App\Pondok;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PondokCollection;

class PondokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\Pondok  $pondok
     * @return \Illuminate\Http\Response
     */
    public function show(Pondok $pondok)
    {
        return new PondokCollection($pondok->with(['city'])->orderBY('name')->paginate(5));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\Pondok  $pondok
     * @return \Illuminate\Http\Response
     */
    public function edit(Pondok $pondok)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\Pondok  $pondok
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pondok $pondok)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\Pondok  $pondok
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pondok $pondok)
    {
        //
    }
}
