<?php

namespace App\Http\Controllers\Api;

use App\MerchantCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MerchantCategoryCollection;

class MerchantCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\MerchantCategory  $merchant_category
     * @return \Illuminate\Http\Response
     */
    public function show(MerchantCategory $merchant_category)
    {
        return new MerchantCategoryCollection($merchant_category->orderBy('name')->paginate(5));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\MerchantCategory  $merchant_category
     * @return \Illuminate\Http\Response
     */
    public function edit(MerchantCategory $merchant_category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\MerchantCategory  $merchant_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MerchantCategory $merchant_category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\MerchantCategory  $merchant_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(MerchantCategory $merchant_category)
    {
        //
    }
}
