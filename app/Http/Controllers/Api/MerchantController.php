<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Pondok;
use App\Merchant;
use App\Kazana\SMS;
use App\Transaction;
use App\Kazana\Helper;
use App\MerchantCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\MerchantPasswordReset;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'merchant_name' => 'required|min:5|max:30',
            'username' => 'required|min:5|max:30',
            'password' => 'required|min:5',
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'phone' => 'required|numeric',
            'merchant_category_id' => 'required|numeric',
            'pondok_id' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if merchant name exists
        $check_merchant = Merchant::where('merchant_name', $request->input('merchant_name'))->count();
        if ( $check_merchant >= 1 )
        {
            return response()->json(['rd'=>'Merchant Name already exists', 'rc' => '500', 'data' => ['Merchant Name already exists']], 200);
        }

        // check if merchant phone exists
        $check_phone = Merchant::where('phone', Helper::format_phone($request->input('phone')))->count();
        if ( $check_phone >= 1 )
        {
            return response()->json(['rd'=>'Phone already exists', 'rc' => '500', 'data' => ['Phone already exists']], 200);
        }

        // check if username exists
        $check_username = Merchant::where('username', $request->input('username'))->count();
        if ( $check_username >= 1 )
        {
            return response()->json(['rd'=>'Username already exists', 'rc' => '500', 'data' => ['Username already exists']], 200);
        }

        // check if pondok exists
        $check_pondok_id = Pondok::where('id', $request->input('pondok_id'))->count();
        if ( $check_pondok_id == 0 )
        {
            return response()->json(['rd'=>'Pondok not exists', 'rc' => '500', 'data' => ['Pondok not exists']], 200);
        }

        // check if merchant category is exists
        $check_merchant_category_id = MerchantCategory::where('id', $request->input('merchant_category_id'))->count();
        if ( $check_merchant_category_id == 0 )
        {
            return response()->json(['rd'=>'Merchant Category not exists', 'rc' => '500', 'data' => ['Merchant Category not exists']], 200);
        }

        // validation success, register merchant
        $new_merchant = new Merchant;
        $new_merchant->merchant_name = $request->input('merchant_name');
        $new_merchant->username = $request->input('username');
        $new_merchant->password = Hash::make($request->input('password'));
        $new_merchant->first_name = $request->input('first_name');
        $new_merchant->last_name = $request->input('last_name');
        $new_merchant->description = $request->input('description');
        $new_merchant->image = '';
        $new_merchant->phone = Helper::format_phone($request->input('phone'));
        $new_merchant->saldo = 0;
        $new_merchant->status = 0; // temporer for testing
        $new_merchant->merchant_category_id = $request->input('merchant_category_id');
        $new_merchant->pondok_id = $request->input('pondok_id');
        $new_merchant->save();

        // then get otp, send via sms thirparty
        $merchant = Merchant::find($new_merchant->id);
        //$data['otp_number'] = $merchant->otp;
        $data['message'] = 'Pendaftaran berhasil, silahkan verifikasi akun Anda';
        $to_phone_number = Helper::format_phone($request->input('phone'));
        if ( $to_phone_number )
        {
            SMS::send($to_phone_number, 'Kazana.id - Kode verifikasi Anda adalah '. $merchant->otp);
        }

        return response()->json(['rd'=>'Pendaftaran berhasil, silahkan verifikasi akun Anda', 'rc' => '200', 'data' => $data], 200);
    }

    public function resend_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'Semua isian harus di isi', 'rc' => '500', 'data' => $data], 200);
        }

        $merchant_otp = Merchant::where('phone', Helper::format_phone($request->input('phone')))->first();

        if ( !$merchant_otp )
        {
            return response()->json(['rd'=>'Nomor telepon tidak terdaftar', 'rc' => '500', 'data' => ['message' => '']], 200);
        }

        if ( $merchant_otp->status == 1 )
        {
            return response()->json(['rd'=>'Nomor telepon sudah ter-verifikasi', 'rc' => '500', 'data' => ['message' => '']], 200);
        }

        // then get otp, send via sms thirparty
        $code_otp = $merchant_otp->otp;
        $data['message'] = 'Pengiriman ulang kode OTP sukses.';
        $to_phone_number = Helper::format_phone($request->input('phone'));
        if ( $to_phone_number )
        {
            SMS::send($to_phone_number, 'Kazana.id - Kode verifikasi Anda adalah '. $code_otp);
        }

        return response()->json(['rd'=>'Pengiriman ulang kode OTP sukses.', 'rc' => '200', 'data' => ['message' => 'Pengiriman ulang kode OTP sukses.']], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Merchant $merchant, Request $request)
    {
        $user_token = $request->header('token');
        $merchant = $merchant->with(['pondok'])->where('token', $user_token)->where('status', 1)->first();

        if ( $merchant == null )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['Merchant not exists']], 200);
        }

        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        $data['merchant_name'] = $merchant->merchant_name;
        $data['username'] = $merchant->username;
        $data['first_name'] = $merchant->first_name;
        $data['last_name'] = $merchant->last_name;
        $data['description'] = $merchant->description;

        $data['alamat'] = $merchant->alamat ?? '';
        $data['desa'] = $merchant->desa ?? '';
        $data['kecamatan'] = $merchant->kecamatan ?? '';
        $data['kota'] = $merchant->kota ?? '';
        $data['provinsi'] = $merchant->provinsi ?? '';

        $data['phone'] = Helper::format_phone($merchant->phone) ?? '';
        $data['fax'] = $merchant->fax ?? '';
        $data['email'] = $merchant->email ?? '';
        $data['website'] = $merchant->website ?? '';

        $data['saldo'] = $merchant->saldo;
        $data['image'] = $merchant->image ? Helper::image_url(600, 600, $merchant->image) : 'http://kazana.dev-support.net/custom/img/user-dummy.png';
        $data['pondok_name'] = $merchant->pondok->name;
        $data['merchant_category_id'] = $merchant->merchant_category_id;

        $data['pimpinan_nama'] = $merchant->pimpinan_nama ?? '';
        $data['pimpinan_alamat'] = $merchant->pimpinan_alamat ?? '';
        $data['pimpinan_desa'] = $merchant->pimpinan_desa ?? '';
        $data['pimpinan_kecamatan'] = $merchant->pimpinan_kecamatan ?? '';
        $data['pimpinan_kota'] = $merchant->pimpinan_kota ?? '';
        $data['pimpinan_provinsi'] = $merchant->pimpinan_provinsi ?? '';
        $data['pimpinan_kodepos'] = $merchant->pimpinan_kodepos ?? '';
        $data['pimpinan_phone'] = Helper::format_phone($merchant->pimpinan_phone) ?? '';
        $data['pimpinan_email'] = $merchant->pimpinan_email ?? '';

        $data['pemilik_nama'] = $merchant->pemilik_nama ?? '';
        $data['pemilik_alamat'] = $merchant->pemilik_alamat ?? '';
        $data['pemilik_desa'] = $merchant->pemilik_desa ?? '';
        $data['pemilik_kecamatan'] = $merchant->pemilik_kecamatan ?? '';
        $data['pemilik_kota'] = $merchant->pemilik_kota ?? '';
        $data['pemilik_provinsi'] = $merchant->pemilik_provinsi ?? '';
        $data['pemilik_kodepos'] = $merchant->pemilik_kodepos ?? '';
        $data['pemilik_phone'] = Helper::format_phone($merchant->pemilik_phone) ?? '';
        $data['pemilik_email'] = $merchant->pemilik_email ?? '';

        $data['created_at'] = date('d F Y', strtotime($merchant->created_at));

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function edit(Merchant $merchant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Merchant $merchant)
    {

        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();

        if ( !$merchant )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['data' => 'tidak ditemukan'] ], 200);
        }

        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        $validator = Validator::make($request->all(), [
            'merchant_name' => 'required|min:5|max:30',
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'phone' => 'required|numeric',
            'merchant_category_id' => 'required|numeric',

            //'alamat' => 'required|min:3',
            //'desa' => 'required|min:3',
            //'kecamatan' => 'required|min:3',
            //'kota' => 'required|min:3',
            //'provinsi' => 'required|min:3',
            //'fax' => 'required|min:3',
            //'email' => 'required|min:3',
            //'website' => 'required|min:3',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if merchant name exists
        $check_merchant = Merchant::where('merchant_name', $request->input('merchant_name'))->where('id', '<>', $merchant_id)->count();
        if ( $check_merchant >= 1 )
        {
            return response()->json(['rd'=>'Merchant Name already exists', 'rc' => '500', 'data' => ['Merchant Name already exists']], 200);
        }

        // check if merchant category is exists
        $check_merchant_category_id = MerchantCategory::where('id', $request->input('merchant_category_id'))->count();
        if ( $check_merchant_category_id == 0 )
        {
            return response()->json(['rd'=>'Merchant Category not exists', 'rc' => '500', 'data' => ['Merchant Category not exists']], 200);
        }

        if ( $request->input('image') && strpos($request->input('image'), 'http://kazana.dev-support.net') === false ) //&& file_exists(storage_path('tmp/'. $request->input('image'))))
        {
            @Storage::move('tmp/'. $request->input('image'), 'public/'. $request->input('image'));
            $new_image = $request->input('image');
        }
        else if (strpos($request->input('image'), 'http://kazana.dev-support.net') !== false && basename($request->input('image')) != 'user-dummy.png')
        {
            $new_image = basename($request->input('image'));
        }
        else
        {
            $new_image = '';
        }

        // validation success, register merchant
        $new_merchant = Merchant::where('id', $merchant_id)->first();
        $new_merchant->merchant_name = $request->input('merchant_name');
        $new_merchant->first_name = $request->input('first_name');
        $new_merchant->last_name = $request->input('last_name');
        $new_merchant->image = $new_image;
        $new_merchant->phone = Helper::format_phone($request->input('phone'));
        $new_merchant->merchant_category_id = $request->input('merchant_category_id');

        $new_merchant->alamat = $request->input('alamat');
        $new_merchant->desa =$request->input('desa');
        $new_merchant->kecamatan = $request->input('kecamatan');
        $new_merchant->kota = $request->input('kota');
        $new_merchant->provinsi = $request->input('provinsi');
        $new_merchant->fax = $request->input('fax');
        $new_merchant->email = $request->input('email');
        $new_merchant->website = $request->input('website');

        $new_merchant->pimpinan_nama = $request->input('pimpinan_nama');
        $new_merchant->pimpinan_alamat = $request->input('pimpinan_alamat');
        $new_merchant->pimpinan_desa = $request->input('pimpinan_desa');
        $new_merchant->pimpinan_kecamatan = $request->input('pimpinan_kecamatan');
        $new_merchant->pimpinan_kota = $request->input('pimpinan_kota');
        $new_merchant->pimpinan_provinsi = $request->input('pimpinan_provinsi');
        $new_merchant->pimpinan_kodepos = $request->input('pimpinan_kodepos');
        $new_merchant->pimpinan_phone = Helper::format_phone($request->input('pimpinan_phone'));
        $new_merchant->pimpinan_email = $request->input('pimpinan_email');

        $new_merchant->pemilik_nama = $request->input('pemilik_nama');
        $new_merchant->pemilik_alamat = $request->input('pemilik_alamat');
        $new_merchant->pemilik_desa = $request->input('pemilik_desa');
        $new_merchant->pemilik_kecamatan = $request->input('pemilik_kecamatan');
        $new_merchant->pemilik_kota = $request->input('pemilik_kota');
        $new_merchant->pemilik_provinsi = $request->input('pemilik_provinsi');
        $new_merchant->pemilik_kodepos = $request->input('pemilik_kodepos');
        $new_merchant->pemilik_phone = Helper::format_phone($request->input('pemilik_phone'));
        $new_merchant->pemilik_email = $request->input('pemilik_email');

        $new_merchant->save();


        $merchant = Merchant::with(['pondok'])->where('id', $merchant_id)->first();

        $data = [];
        $data['merchant_name'] = $merchant->merchant_name;
        $data['username'] = $merchant->username;
        $data['first_name'] = $merchant->first_name;
        $data['last_name'] = $merchant->last_name;
        $data['description'] = $merchant->description;
        $data['phone'] = Helper::format_phone($merchant->phone);
        $data['saldo'] = $merchant->saldo;
        $data['image'] = $merchant->image ? Helper::image_url(600, 600, $merchant->image) : 'http://kazana.dev-support.net/custom/img/user-dummy.png';
        $data['pondok_name'] = $merchant->pondok->name;
        $data['merchant_category_id'] = $merchant->merchant_category_id;

        $data['alamat'] = $merchant->alamat;
        $data['desa'] =$merchant->desa;
        $data['kecamatan'] = $merchant->kecamatan;
        $data['kota'] = $merchant->kota;
        $data['provinsi'] = $merchant->provinsi;
        $data['fax'] = $merchant->fax;
        $data['email'] = $merchant->email;
        $data['website'] = $merchant->website;
        $data['created_at'] = date('d F Y', strtotime($merchant->created_at));

        $data['pimpinan_nama'] = $merchant->pimpinan_nama ?? '';
        $data['pimpinan_alamat'] = $merchant->pimpinan_alamat ?? '';
        $data['pimpinan_desa'] = $merchant->pimpinan_desa ?? '';
        $data['pimpinan_kecamatan'] = $merchant->pimpinan_kecamatan ?? '';
        $data['pimpinan_kota'] = $merchant->pimpinan_kota ?? '';
        $data['pimpinan_provinsi'] = $merchant->pimpinan_provinsi ?? '';
        $data['pimpinan_kodepos'] = $merchant->pimpinan_kodepos ?? '';
        $data['pimpinan_phone'] = Helper::format_phone($merchant->pimpinan_phone) ?? '';
        $data['pimpinan_email'] = $merchant->pimpinan_email ?? '';

        $data['pemilik_nama'] = $merchant->pemilik_nama ?? '';
        $data['pemilik_alamat'] = $merchant->pemilik_alamat ?? '';
        $data['pemilik_desa'] = $merchant->pemilik_desa ?? '';
        $data['pemilik_kecamatan'] = $merchant->pemilik_kecamatan ?? '';
        $data['pemilik_kota'] = $merchant->pemilik_kota ?? '';
        $data['pemilik_provinsi'] = $merchant->pemilik_provinsi ?? '';
        $data['pemilik_kodepos'] = $merchant->pemilik_kodepos ?? '';
        $data['pemilik_phone'] = Helper::format_phone($merchant->pemilik_phone) ?? '';
        $data['pemilik_email'] = $merchant->pemilik_email ?? '';

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merchant $merchant)
    {
        //
    }

    public function logout(Request $request)
    {
        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();

        if ( $merchant == null )
        {
            return response()->json(['rd'=>'Merchant not exists', 'rc' => '200', 'data' => ['Merchant not exists']], 200);
        }

        $merchant->token = '';
        $merchant->save();

        return response()->json(['rd'=>'Berhasil keluar dari Aplikasi', 'rc' => '200', 'data' => []], 200);        
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:5|max:30',
            'password' => 'required|min:5',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if merchant username exists
        $check_merchant = Merchant::where('username', $request->input('username'));
        if ( $check_merchant->count() == 0 )
        {
            return response()->json(['rd'=>'Username tidak terdaftar', 'rc' => '500', 'data' => ['Username tidak terdaftar']], 200);
        }

        $merchant = $check_merchant->first();

        if ( $merchant->status == 0 )
        {
            return response()->json(['rd'=>'Akun Anda belum ter-verifikasi', 'rc' => '500', 'data' => ['Akun Anda belum ter-verifikasi']], 200);
        }

        if (Hash::check($request->input('password'), $merchant->password))
        {
            $data['token'] = $merchant->id . Str::random(32);
            $merchant->token = $data['token'];
            $merchant->save();
        }
        else
        {
            return response()->json(['rd'=>'Password tidak benar', 'rc' => '500', 'data' => ['Password tidak benar']], 200);
        }

        return response()->json(['rd'=>'Login Berhasil!', 'rc' => '200', 'data' => $data], 200);        
    }

    public function otp_confirm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required|min:6',
            'username' => 'required|min:5|max:30',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        // check if otp exists
        $check_merchant = Merchant::where('otp', $request->input('otp'))->where('username', $request->input('username'));
        if ( $check_merchant->count() == 0 )
        {
            return response()->json(['rd'=>'Kode tidak ditemukan', 'rc' => '500', 'data' => ['Kode tidak ditemukan']], 200);
        }

        $merchant = $check_merchant->first();

        if ( $merchant->status == 1 )
        {
            return response()->json(['rd'=>'Akun Anda sudah diaktivasi', 'rc' => '500', 'data' => ['Akun Anda sudah diaktivasi']], 200);
        }

        $merchant->status = 1;
        $merchant->save();

        return response()->json(['rd'=>'Verifikasi berhasil', 'rc' => '200', 'data' => ['message' => 'Verifikasi berhasil']], 200);
    }

    public function dashboard(Request $request)
    {
        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();
        $merchant_id = $merchant->id;
        $pondok_id = $merchant->pondok_id;

        $current = Transaction::where('status', 1)->where('merchant_id', $merchant_id);
        $yesterday = clone($current);
        $this_month = clone($current);

        $data['today'] = 0;
        $data['yesterday'] = 0;
        $data['this_month'] = 0;

        $date_today = date('Y-m-d');
        $current = $current->selectRaw('SUM(amount) as total')->whereRaw('DATE(created_at)=CURDATE()')->first();
        $data['today'] = $current->total ?? 0;

        $yesterday = $yesterday->selectRaw('SUM(amount) as total')->whereRaw('DATE(created_at)=DATE_SUB(CURDATE(), INTERVAL 1 DAY)')->first();
        $data['yesterday'] = $yesterday->total ?? 0;

        $this_month = $this_month->selectRaw('SUM(amount) as total')->whereRaw('YEAR(created_at)=YEAR(NOW()) AND MONTH(created_at)=MONTH(NOW())')->first();
        $data['this_month'] = $this_month->total ?? 0;

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => $data], 200);
    }

    public function password_update(Request $request)
    {
        $user_token = $request->header('token');
        $merchant = Merchant::where('token', $user_token)->where('status', 1)->first();
        $merchant_id = $merchant->id;

        $validator = Validator::make($request->all(), [
            'password_old' => 'required|min:5',
            'password_new' => 'required|min:5',
            'password_new2' => 'required|min:5',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        if ( $request->input('password_new') != $request->input('password_new2') )
        {
            return response()->json(['rd'=>'Password baru tidak cocok', 'rc' => '500', 'data' => ['message' => 'Password baru tidak cocok']], 200);
        }

        if (Hash::check($request->input('password_old'), $merchant->password))
        {
            $new_merchant = Merchant::find($merchant->id);
            $new_merchant->password = Hash::make($request->input('password_new'));
            $new_merchant->save();

            return response()->json(['rd'=>'Password sukses diubah', 'rc' => '200', 'data' => ['message' => 'Password sukses diubah']], 200);
        }
        else
        {
            return response()->json(['rd'=>'Password salah', 'rc' => '500', 'data' => ['Password salah']], 200);
        }

    }

    public function forgot(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        $merchant = Merchant::where('phone', Helper::format_phone($request->input('phone')))->where('status', 1)->first();

        if ( !$merchant )
        {
            return response()->json(['rd'=>'Nomor telepon tidak ditemukan', 'rc' => '500', 'data' => ['message' => 'Nomor telepon tidak ditemukan']], 200);            
        }

        if ( $merchant->phone != Helper::format_phone($request->input('phone')) )
        {
            return response()->json(['rd'=>'Nomor telepon tidak ditemukan', 'rc' => '500', 'data' => ['message' => 'Nomor telepon tidak ditemukan']], 200);
        }

        $pin_password_reset = Helper::generatePIN();

        $new_data = new MerchantPasswordReset;
        $new_data->merchant_id = $merchant->id;
        $new_data->phone = Helper::format_phone($request->input('phone'));
        $new_data->token = $pin_password_reset;
        $new_data->status = 0;
        $new_data->created_at = date('Y-m-d H:i:s');
        $new_data->save();

        // langsung update password, dan password baru dikirim via sms
        $new_password_reset = Helper::generatePIN();

        $new_merchant = Merchant::where('phone', Helper::format_phone($request->input('phone')))->where('status', 1)->first();
        $new_merchant->password = Hash::make($new_password_reset);
        $new_merchant->save();
        // end

        // send sms new password
        $to_phone_number = Helper::format_phone($request->input('phone'));
        if ( $to_phone_number )
        {
            SMS::send($to_phone_number, 'Kazana.id - Password baru Anda adalah '. $new_password_reset .'. Segera ganti password Anda!');
        }

        return response()->json(['rd'=>'Password baru sudah dikirim ke nomor HP Anda', 'rc' => '200', 'data' => ['message' => 'Password baru sudah dikirim ke nomor HP Anda ']], 200);
    }

    public function pin_check(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric',
            'token' => 'required|numeric',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        $password_reset = MerchantPasswordReset::where('phone', Helper::format_phone($request->input('phone')))->where('token', $request->input('token'))->whereRaw("DATE_SUB(NOW(), INTERVAL 1 DAY) <= created_at")->where('status', 0)->first();

        if ( $password_reset )
        {
            return response()->json(['rd'=>'success', 'rc' => '200', 'data' => ['message' => 'PIN reset password terdaftar']], 200);
        }
        else
        {
            return response()->json(['rd'=>'success', 'rc' => '500', 'data' => ['message' => 'PIN reset password tidak ditemukan']], 200);            
        }
    }

    public function password_reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric',
            'token' => 'required|numeric',
            'password_new' => 'required|min:5',
            'password_new2' => 'required|min:5',
        ]);

        $data = [];

        if ($validator->fails()) 
        {
            $errors = $validator->errors();

            foreach ($errors->all() as $key => $message) 
            {
                $data[$key] = $message;
            }

            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => $data], 200);
        }

        if ( $request->input('password_new') != $request->input('password_new2') )
        {
            return response()->json(['rd'=>'error', 'rc' => '500', 'data' => ['message' => 'Password baru tidak cocok']], 200);
        }

        $password_reset = MerchantPasswordReset::where('phone', Helper::format_phone($request->input('phone')))->where('token', $request->input('token'))->whereRaw("DATE_SUB(NOW(), INTERVAL 1 DAY) <= created_at")->where('status', 0)->first();

        if ( !$password_reset )
        {
            return response()->json(['rd'=>'success', 'rc' => '500', 'data' => ['message' => 'PIN reset password tidak ditemukan']], 200);            
        }

        $new_merchant = Merchant::find($password_reset->merchant_id);
        $new_merchant->password = Hash::make($request->input('password_new'));
        $new_merchant->save();

        $password_reset->status = 1;
        $password_reset->save();

        return response()->json(['rd'=>'success', 'rc' => '200', 'data' => ['message' => 'Password sukses diubah']], 200);
    }

    public function testsms(Request $request)
    {
        SMS::send('6285288777998', 'Kazana.id - Kode PIN verifikasi Anda adalah 336699');
    }
}
