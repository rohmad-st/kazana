<?php

namespace App\Http\Controllers\Api;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\FaqCollection;

class PageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Api\PageController  $page
     * @return \Illuminate\Http\Response
     */
    public function faq(Page $page)
    {
        return new FaqCollection($page->where('id', 1)->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\PageController  $page
     * @return \Illuminate\Http\Response
     */
    public function term(Page $page)
    {
        return new FaqCollection($page->where('id', 2)->get());
    }
}
