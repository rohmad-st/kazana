<?php

namespace App\Http\Controllers;

use App\Santri;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class SantriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function show(Santri $santri)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function edit(Santri $santri)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Santri $santri)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Santri  $santri
     * @return \Illuminate\Http\Response
     */
    public function destroy(Santri $santri)
    {
        //
    }

    public function cardQr(Request $request, $id)
    {
        $row = Santri::findOrFail($id);

        return \QRCode::text($row->barcode)->setMargin(0)->setSize(6)->png();
    }

    public function cardPrint(Request $request, $id)
    {
        $is_download = $request->input('download');

        $row = Santri::with(['pondok'])->where('id', $id)->first();

        \QRCode::text($row->barcode)->setMargin(0)->setSize(8)->setOutfile(public_path('custom/qrcode/'. $row->id .'.png'))->png();

        $img = Image::make(public_path('custom/img/Kartu_Santri_Kazana_Depan_blank.png'));
        
        $img->text($row->name, 110, 250, function($font) {  
              $font->file(public_path('custom/font/AVENIRLTSTD-HEAVY.OTF'));  
              $font->size(40);  
              $font->color('#0389FF');  
              $font->align('left');  
              $font->valign('bottom');  
              $font->angle(0);
        });  
        $img->text($row->nisn, 110, 280, function($font) {  
              $font->file(public_path('custom/font/AvenirLTStd-Roman.otf'));
              $font->size(30);  
              $font->color('#0389FF');  
              $font->align('left');  
              $font->valign('bottom');  
              $font->angle(0);
        });  

        if ( is_file(storage_path('app/public/'. $row->image)))
        {
            // draw filled red rectangle
            $img->rectangle(110, 330, 350, 570, function ($draw) {
                $draw->background('#FFFFFF');
            });
            $img_avatar = Image::make(storage_path('app/public/'.$row->image));
            $img_avatar->resize(230, 220);
            $img->insert($img_avatar, 'bottom-left', 110, 110);
        }

        // draw filled red rectangle
        $img->rectangle(710, 330, 950, 570, function ($draw) {
            $draw->background('#FFFFFF');
        });        

        $img->insert('custom/qrcode/'. $row->id .'.png', 'bottom-right', 130, 130);

        if ( $is_download == 1 )
        {
            $filepath = storage_path('app/public/card_id_'. $row->id .'.jpg');
            $img->save($filepath);
            return response()->download($filepath);
        }
        else
        {
            return $img->response('jpg');        
        }
    }

    public function cardBackPrint(Request $request)
    {
        $img = Image::make(public_path('custom/img/Kartu_Santri_Kazana_Belakang_blank.png'));
        $point1 = '1. Kartu ini hanya bisa digunakan di Pondok Pesantren Tambak Beras dan oleh nama yang';
        $point1_a = 'tercantum di kartu ini';

        $point2 = '2. Jika Kartu ini hilang / rusak, pemilik kartu bisa datang ke Kantor Pondok Pesantren';
        $point2_a = 'untuk melaporkan kepada Guru';

        $point3 = '3. Kartu ini milik Pondok Pesantren Tambak Beras jika menemukan kartu ini bisa menghubungi ';
        $point3_a = 'Pondok Pesantren Tambak Beras';
        $point3_b = 'Dsn Tambak Beras, Desa Tambak Rejo';
        $point3_c = 'Jombang - Jawa Timur';

        return $img->response('jpg');
    }

    public function cardAll(Request $request)
    {
        $rows = Santri::with(['pondok']);

        if ( \Auth::user()->pondok_id > 0 )
        {
            $rows->where('pondok_id', \Auth::user()->pondok_id);
        }
        $rows = $rows->get();

        return view('card-all', ['rows' => $rows]);
    }
}
