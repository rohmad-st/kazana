<?php

namespace App\Http\Controllers;

use App\PaymentTopup;
use Illuminate\Http\Request;

class PaymentTopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentTopup  $paymentTopup
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentTopup $paymentTopup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentTopup  $paymentTopup
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentTopup $paymentTopup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentTopup  $paymentTopup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentTopup $paymentTopup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentTopup  $paymentTopup
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentTopup $paymentTopup)
    {
        //
    }
}
