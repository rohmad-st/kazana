<?php
namespace App\Http\Controllers;

use App\Ortu;
use Illuminate\Http\Request;

class TopupController extends Controller
{
    public function index()
    {
        return view('topup/index');
    }

    public function topup_atm()
    {
        return view('topup/topup_atm');
    }

    public function topup_bca(Request $request)
    {
        $phone = $this->_get_phone_number_ortu($request);

        return view('topup/topup_bca', ['phone' => $phone]);
    }

    public function topup_mandiri(Request $request)
    {
        $phone = $this->_get_phone_number_ortu($request);

        return view('topup/topup_mandiri', ['phone' => $phone]);
    }

    public function topup_mbanking()
    {
        return view('topup/topup_mbanking');
    }

    public function topup_mbca(Request $request)
    {
        $phone = $this->_get_phone_number_ortu($request);

        return view('topup/topup_mbca', ['phone' => $phone]);
    }

    public function topup_mmandiri(Request $request)
    {
        $phone = $this->_get_phone_number_ortu($request);

        return view('topup/topup_mmandiri', ['phone' => $phone]);
    }

    public function topup_banklain()
    {
        return view('topup/topup_banklain');
    }

    private function _get_phone_number_ortu(Request $request)
    {
        $user_token = $request->header('token');

        if ( $user_token )
        {
            $ortu = Ortu::where('token', $user_token)->where('status', 1)->first();

            if ( !$ortu )
            {
                return '';
            }

            $phone = $ortu->phone;

            return $phone ?? '';
        }

        return '';
    }
}