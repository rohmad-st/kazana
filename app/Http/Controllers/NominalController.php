<?php

namespace App\Http\Controllers;

use App\Nominal;
use Illuminate\Http\Request;

class NominalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nominal  $nominal
     * @return \Illuminate\Http\Response
     */
    public function show(Nominal $nominal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nominal  $nominal
     * @return \Illuminate\Http\Response
     */
    public function edit(Nominal $nominal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nominal  $nominal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nominal $nominal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nominal  $nominal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nominal $nominal)
    {
        //
    }

    public function nominalQr(Request $request, $id)
    {
        $row = Nominal::findOrFail($id);

        return \QRCode::text($row->barcode)->setMargin(0)->setSize(6)->png();
    }

    public function nominalAll(Request $request)
    {
        $rows = Nominal::all();

        return view('nominal-all', ['rows' => $rows]);
    }

}
