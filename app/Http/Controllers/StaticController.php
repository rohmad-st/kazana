<?php
namespace App\Http\Controllers;

use App\Page;

class StaticController extends Controller
{
    public function faq()
    {
        $faq = Page::find(1);

        return view('static/faq', ['faq' => $faq]);
    }

    public function tos()
    {
        $tos = Page::find(2);

        return view('static/tos', ['tos' => $tos]);
    }

    public function tutorial()
    {
        $tutorial = Page::find(3);

        return view('static/tutorial', ['tutorial' => $tutorial]);        
    }

    public function privacy_policy()
    {
        $privacy_policy = Page::find(4);

        return view('static/privacy_policy', ['privacy_policy' => $privacy_policy]);        
    }

    public function receive_dn(Request $request)
    {
        $data = $request->getContent();

        file_put_contents(storage_path('app/sms_receive.txt'), $data ."\n");
    }
}