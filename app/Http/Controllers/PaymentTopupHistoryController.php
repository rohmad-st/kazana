<?php

namespace App\Http\Controllers;

use App\PaymentTopupHistory;
use Illuminate\Http\Request;

class PaymentTopupHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentTopupHistory  $paymentTopupHistory
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentTopupHistory $paymentTopupHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentTopupHistory  $paymentTopupHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentTopupHistory $paymentTopupHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentTopupHistory  $paymentTopupHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentTopupHistory $paymentTopupHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentTopupHistory  $paymentTopupHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentTopupHistory $paymentTopupHistory)
    {
        //
    }
}
