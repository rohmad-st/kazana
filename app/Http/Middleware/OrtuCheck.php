<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Cache;
use App\Ortu;
use Closure;

class OrtuCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_token = $request->header('token');

        $check_ortu = Ortu::where('token', $user_token)->where('status', 1)->first();

        if ( $check_ortu && $user_token && ($check_ortu->token == $user_token ))
        {
        }
        else
        {
            return response()->json(['rd'=>'Silahkan login', 'rc' => '401', 'data' => ['Silahkan login']], 200);
            //abort(401);
        }

        return $next($request);
    }
}
