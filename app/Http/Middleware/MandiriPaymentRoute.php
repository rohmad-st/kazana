<?php

namespace App\Http\Middleware;

use Closure;

class MandiriPaymentRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $compareToken = '8bfd13377984f7871b06686d4ee2fa96b4cf219c7566712fcd36e87bf4c5a865cc73de5f20e27508256a6f60f4ea014a';
        $token        = $request->header('Authorization');

        if(empty($token) || !hash_equals($token, $compareToken)) {
            return response()->json('Unauthorize', 401);
        }
        return $next($request);
    }
}
