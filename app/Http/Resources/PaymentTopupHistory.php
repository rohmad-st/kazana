<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentTopupHistory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "payment_topup_name" => $this->payment_topup->name,
            "ortu_id" => $this->ortu->name,
            "amount" => $this->amount,
            "created_at" => date('Y-m-d H:i:s', strtotime($this->created_at)),
        ];        
    }
}
