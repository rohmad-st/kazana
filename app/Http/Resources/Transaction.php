<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Transaction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "invoice" => $this->invoice,
            "amount" => $this->amount ?? 0,
            "note" => $this->note ?? '',
            "created_at" => date('Y-m-d H:i:s', strtotime($this->created_at)),
            "saldo_balance" => $this->saldo_balance ?? 0,
            "santri_nisn" => $this->santri->nisn,
            "santri_name" => $this->santri->name,
            "pondok_name" => $this->pondok->name,
        ];
    }

}
