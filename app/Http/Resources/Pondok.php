<?php

namespace App\Http\Resources;

use App\Kazana\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class Pondok extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "phone" => $this->phone,
            "description" => $this->description,
            "image" => Helper::image_url(512, 512, $this->image),
            "address" => $this->address,
            "city" => $this->city->name,
        ];
    }
}
