<?php

namespace App\Http\Resources;

use App\Kazana\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentTopup extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "image" => Helper::image_url(512, 512, $this->image),
        ];
    }
}
