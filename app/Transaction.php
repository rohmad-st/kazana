<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Silvanite\Brandenburg\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasRoles;
    use SoftDeletes;

    protected $dates = [
      'deleted_at',
    ];


    public function pondok()
    {
        return $this->belongsTo('App\Pondok', 'pondok_id', 'id');
    }

    public function santri()
    {
        return $this->belongsTo('App\Santri', 'santri_id', 'id');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Merchant', 'merchant_id', 'id');
    }
}
