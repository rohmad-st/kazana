<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landingpage');
});

Route::get('/policy', function () {
	return view('policy');
});

// Route::get('/', function () {
//     return redirect('/admin');
// });

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/card-santri/{id}', 'SantriController@cardPrint')->where(['id' => '[0-9]+']);
Route::get('/card-qr/{id}', 'SantriController@cardQr')->where(['id' => '[0-9]+']);
Route::get('/card-all', 'SantriController@cardAll');
Route::get('/card-santri-back', 'SantriController@cardBackPrint');

Route::get('/nominal-qr/{id}', 'NominalController@nominalQr')->where(['id' => '[0-9]+']);
Route::get('/nominal-all', 'NominalController@nominalAll');

// top up
Route::get('/topup', 'TopupController@index');
Route::get('/topup/atm', 'TopupController@topup_atm');
Route::get('/topup/bca', 'TopupController@topup_bca');
Route::get('/topup/mandiri', 'TopupController@topup_mandiri');

Route::get('/topup/mbanking', 'TopupController@topup_mbanking');
Route::get('/topup/mbca', 'TopupController@topup_mbca');
Route::get('/topup/mmandiri', 'TopupController@topup_mmandiri');
Route::get('/topup/bank-lain', 'TopupController@topup_banklain');

// static page
Route::get('/faq', 'StaticController@faq');
Route::get('/tos', 'StaticController@tos');
Route::get('/tutorial', 'StaticController@tutorial');
Route::get('/privacy', 'StaticController@privacy_policy');

// SMS
Route::get('/receive_dn', 'StaticController@receive_dn');

/* eof */
