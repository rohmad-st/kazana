<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/city', 'Api\CityController@show');
Route::get('/pondok', 'Api\PondokController@show');

Route::get('/payment', 'Api\PaymentTopupController@show');
Route::post('/payment/{nisn}', 'Api\PaymentTopupController@store');

Route::get('/faq', 'Api\PageController@faq');
Route::get('/term', 'Api\PageController@term');
Route::post('/upload-file', 'Api\UploadController@endpoint');

Route::get('/merchant-sms-test', 'Api\MerchantController@testsms');

Route::get('/merchant-category', 'Api\MerchantCategoryController@show');
Route::post('/merchant-register', 'Api\MerchantController@store');
Route::post('/merchant-resend-top-register', 'Api\MerchantController@resend_otp');
Route::post('/merchant-login', 'Api\MerchantController@login');
Route::post('/merchant-otp-confirm', 'Api\MerchantController@otp_confirm');
Route::post('/merchant-forgot', 'Api\MerchantController@forgot');
Route::post('/merchant-pin-check', 'Api\MerchantController@pin_check');
Route::post('/merchant-reset-password', 'Api\MerchantController@password_reset');

Route::group(['middleware' => ['merchant']], function () {
	Route::post('/nominal-scan', 'Api\SantriController@scan_nominal');
	Route::post('/santri-scan', 'Api\SantriController@scan_santri');
	Route::post('/merchant-dashboard', 'Api\MerchantController@dashboard');
	Route::post('/merchant-profile', 'Api\MerchantController@show');
	Route::post('/merchant-update', 'Api\MerchantController@update');
	Route::post('/merchant-logout', 'Api\MerchantController@logout');
	Route::post('/merchant-history', 'Api\TransactionController@history');
	Route::post('/transaction-detail', 'Api\TransactionController@order');
	Route::post('/transaction-confirm', 'Api\TransactionController@confirm');
	Route::post('/notification', 'Api\NotificationController@history');
	Route::post('/update_notification', 'Api\NotificationController@update_status_history');
	Route::post('/merchant-update-password', 'Api\MerchantController@password_update');
});

Route::post('/ortu-register', 'Api\OrtuController@store');
Route::post('/ortu-otp-confirm', 'Api\OrtuController@otp_confirm');
Route::post('/ortu-login', 'Api\OrtuController@login');
Route::post('/ortu-resend-top-register', 'Api\OrtuController@resend_otp');

Route::group(['middleware' => ['ortu']], function () {
	Route::post('/ortu-logout', 'Api\OrtuController@logout');
	Route::post('/ortu-profile', 'Api\OrtuController@show');
	Route::post('/ortu-notification', 'Api\NotificationController@ortu_history');
	Route::post('/ortu-update_notification', 'Api\NotificationController@update_status_ortu_history');
	Route::post('/ortu-history', 'Api\TransactionController@ortu_history');
	Route::post('/ortu-dashboard', 'Api\OrtuController@dashboard');
	Route::post('/ortu-update', 'Api\OrtuController@update');
	Route::post('/ortu-update-password', 'Api\OrtuController@password_update');
	Route::post('/ortu-payment-history', 'Api\OrtuController@payment_history');
});
