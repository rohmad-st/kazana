<?php return array (
  'benjaminhirsch/nova-slug-field' => 
  array (
    'providers' => 
    array (
      0 => 'Benjaminhirsch\\NovaSlugField\\FieldServiceProvider',
    ),
  ),
  'beyondcode/laravel-dump-server' => 
  array (
    'providers' => 
    array (
      0 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    ),
  ),
  'chumper/zipper' => 
  array (
    'providers' => 
    array (
      0 => 'Chumper\\Zipper\\ZipperServiceProvider',
    ),
    'aliases' => 
    array (
      'Zipper' => 'Chumper\\Zipper\\Zipper',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'kazana/qr-setting' => 
  array (
    'providers' => 
    array (
      0 => 'Kazana\\QrSetting\\ToolServiceProvider',
    ),
  ),
  'kazana/qrcode-santri' => 
  array (
    'providers' => 
    array (
      0 => 'Kazana\\QrcodeSantri\\ToolServiceProvider',
    ),
  ),
  'kazana/santri_import_photo' => 
  array (
    'providers' => 
    array (
      0 => 'Kazana\\SantriImportPhoto\\CardServiceProvider',
    ),
  ),
  'kristories/nova-qrcode-field' => 
  array (
    'providers' => 
    array (
      0 => 'Kristories\\Qrcode\\FieldServiceProvider',
    ),
  ),
  'laravel/nexmo-notification-channel' => 
  array (
    'providers' => 
    array (
      0 => 'Illuminate\\Notifications\\NexmoChannelServiceProvider',
    ),
  ),
  'laravel/nova' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Nova\\NovaCoreServiceProvider',
    ),
    'aliases' => 
    array (
      'Nova' => 'Laravel\\Nova\\Nova',
    ),
  ),
  'laravel/slack-notification-channel' => 
  array (
    'providers' => 
    array (
      0 => 'Illuminate\\Notifications\\SlackChannelServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'maatwebsite/excel' => 
  array (
    'providers' => 
    array (
      0 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
    ),
    'aliases' => 
    array (
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
    ),
  ),
  'maatwebsite/laravel-nova-excel' => 
  array (
    'providers' => 
    array (
      0 => 'Maatwebsite\\LaravelNovaExcel\\LaravelNovaExcelServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'silvanite/brandenburg' => 
  array (
    'providers' => 
    array (
      0 => 'Silvanite\\Brandenburg\\Providers\\BrandenburgServiceProvider',
    ),
    'aliases' => 
    array (
      'BrandenburgPolicy' => 'Silvanite\\Brandenburg\\Facades\\PolicyFacade',
    ),
  ),
  'silvanite/novafieldcheckboxes' => 
  array (
    'providers' => 
    array (
      0 => 'Silvanite\\NovaFieldCheckboxes\\FieldServiceProvider',
    ),
  ),
  'silvanite/novatoolpermissions' => 
  array (
    'providers' => 
    array (
      0 => 'Silvanite\\NovaToolPermissions\\Providers\\AuthServiceProvider',
      1 => 'Silvanite\\NovaToolPermissions\\Providers\\PackageServiceProvider',
    ),
  ),
  'sparclex/nova-import-card' => 
  array (
    'providers' => 
    array (
      0 => 'Sparclex\\NovaImportCard\\CardServiceProvider',
    ),
  ),
  'werneckbh/laravel-qr-code' => 
  array (
    'providers' => 
    array (
      0 => 'LaravelQRCode\\Providers\\QRCodeServiceProvider',
    ),
    'aliases' => 
    array (
      'QRCode' => 'LaravelQRCode\\Facades\\QRCode',
    ),
  ),
);