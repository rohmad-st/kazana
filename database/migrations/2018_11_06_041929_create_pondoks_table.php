<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePondoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pondoks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->text('address')->nullable();

            // administration additional
            $table->string('nspp')->nullable();
            $table->string('desa')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kota')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('ponpes_kepala')->nullable();
            $table->string('ponpes_alamat')->nullable();
            $table->string('ponpes_telepon')->nullable();
            $table->string('ponpes_email')->nullable();
            $table->string('yayasan_nama')->nullable();
            $table->string('yayasan_alamat')->nullable();
            $table->string('yayasan_telepon')->nullable();
            $table->string('yayasan_email')->nullable();

            $table->integer('city_id');
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pondoks');
    }
}
