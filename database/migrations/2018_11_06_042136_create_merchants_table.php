<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant_name');
            $table->string('username');
            $table->string('password');
            $table->string('first_name');
            $table->string('last_name');
            $table->text('description')->nullable();
            $table->string('phone');
            $table->integer('saldo')->nullable();
            $table->string('image')->nullable();
            $table->string('otp');
            $table->integer('status');

            // administration additional
            $table->string('no_registrasi')->nullable();
            $table->string('alamat')->nullable();
            $table->string('desa')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kota')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('pimpinan_nama');
            $table->string('pimpinan_alamat');
            $table->string('pimpinan_desa')->nullable();
            $table->string('pimpinan_kecamatan')->nullable();
            $table->string('pimpinan_kota')->nullable();
            $table->string('pimpinan_provinsi')->nullable();
            $table->string('pimpinan_kodepos')->nullable();
            $table->string('pimpinan_phone')->nullable();
            $table->string('pimpinan_email')->nullable();
            $table->string('pemilik_nama');
            $table->string('pemilik_alamat');
            $table->string('pemilik_desa')->nullable();
            $table->string('pemilik_kecamatan')->nullable();
            $table->string('pemilik_kota')->nullable();
            $table->string('pemilik_provinsi')->nullable();
            $table->string('pemilik_kodepos')->nullable();
            $table->string('pemilik_phone')->nullable();
            $table->string('pemilik_email')->nullable();

            $table->integer('merchant_category_id');
            $table->integer('pondok_id');
            $table->string('token')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
