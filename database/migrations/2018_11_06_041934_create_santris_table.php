<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSantrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('santris', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode')->nullable();
            $table->string('nisn');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('pin');
            $table->string('image')->nullable();
            $table->integer('saldo');
            $table->integer('no_virtual_account')->nullable();
            $table->integer('transaction_daily_limit')->nullable();
            $table->integer('transaction_today_total')->nullable();

            // administration additional
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->integer('kelamin')->nullable();
            $table->integer('anak_ke')->nullable();
            $table->integer('jumlah_saudara')->nullable();
            $table->string('golongan_darah')->nullable();
            $table->string('asal_sekolah')->nullable();
            $table->string('asal_sekolah_alamat')->nullable();
            $table->string('asal_sekolah_desa')->nullable();
            $table->string('asal_sekolah_kecamatan')->nullable();
            $table->string('asal_sekolah_kota')->nullable();
            $table->string('asal_sekolah_provinsi')->nullable();
            $table->string('nomor_un')->nullable();
            $table->string('nomor_skhun')->nullable();
            $table->string('mata_pelajaran')->nullable();
            $table->string('nilai')->nullable();
            $table->string('nama_sekolah')->nullable();
            $table->string('diterima_dikelas')->nullable(); // I/VII/X | II/VIII/XI | III/IX/XII | IV | V | VI
            $table->string('nama_pondok')->nullable();
            $table->string('nama_kamar')->nullable();
            $table->boolean('punya_saudara_ponpes')->nullable();
            $table->integer('jumlah_saudara_ponpes')->nullable();
            $table->string('prestasi_nama')->nullable();
            $table->string('prestasi_tingkat')->nullable();
            $table->string('prestasi_tahun')->nullable(); // KOTAMADYA/KABUPATEN | PROVINSI | NASIONAL
            $table->string('sumber_informasi')->nullable(); // TELEVISI | INTERNET/WEBSITE | RADIO | MAJALAH/KORAN | KELUARGA DEKAT | TEMAN | TETANGGA | LAINNYA

            $table->integer('pondok_id');
            $table->boolean('status')->default(0);
            $table->string('delete_reason')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('santris');
    }
}
