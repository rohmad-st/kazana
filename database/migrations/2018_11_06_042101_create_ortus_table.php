<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrtusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ortus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('username');
            $table->string('password');
            $table->string('otp');

            // administration additional
            $table->string('masih_hidup')->nullable(); // MASIH HIDUP | WAFAT
            $table->string('pendidikan')->nullable(); // SD/MI SEDERAJAD | SMP/MTs SDERAJAD | SMA/MA SEDERAJAD | PERGURUAN TINGGI
            $table->string('pekerjaan')->nullable(); // BURUH TANI | TANI | WIRASWASTA | PNS | TNI/POLRI | LAINNYA
            $table->string('penghasilan')->nullable(); // < 1.000.000 | 1.000.000 - 2.000.000 | > 2.000.000 
            $table->string('ibu_nama')->nullable();
            $table->string('ibu_masih_hidup')->nullable(); // MASIH HIDUP | WAFAT
            $table->string('ibu_pendidikan')->nullable(); // SD/MI SEDERAJAD | SMP/MTs SDERAJAD | SMA/MA SEDERAJAD | PERGURUAN TINGGI
            $table->string('ibu_pekerjaan')->nullable(); // BURUH TANI | TANI | WIRASWASTA | PNS | TNI/POLRI | LAINNYA
            $table->string('ibu_penghasilan')->nullable(); // < 1.000.000 | 1.000.000 - 2.000.000 | > 2.000.000 
            $table->string('alamat')->nullable();
            $table->string('desa')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kota')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kodepos')->nullable();
            $table->string('nomor_telepon_rumah')->nullable();
            $table->string('nomor_telepon_ayah')->nullable();
            $table->string('nomor_telepon_ibu')->nullable();
            $table->string('image')->nullable();

            $table->integer('santri_id');
            $table->integer('pondok_id');
            $table->string('token')->nullable();
            $table->integer('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ortus');
    }
}
