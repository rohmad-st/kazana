-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2019 at 09:34 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kazana`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_events`
--

CREATE TABLE `action_events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_id` int(10) UNSIGNED NOT NULL,
  `target_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` int(10) UNSIGNED DEFAULT NULL,
  `fields` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'running',
  `exception` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `action_events`
--

INSERT INTO `action_events` (`id`, `batch_id`, `user_id`, `name`, `actionable_type`, `actionable_id`, `target_type`, `target_id`, `model_type`, `model_id`, `fields`, `status`, `exception`, `created_at`, `updated_at`) VALUES
(64, '8f20ceb6-475a-4156-a6e1-a993b7a584b1', 1, 'Update', 'App\\Santri', 6, 'App\\Santri', 6, 'App\\Santri', 6, '', 'finished', '', '2019-11-13 22:51:37', '2019-11-13 22:51:37'),
(65, '8f20d082-cfe6-41e6-9ab6-ca69960ad4ae', 3, 'Create', 'App\\Santri', 27, 'App\\Santri', 27, 'App\\Santri', 27, '', 'finished', '', '2019-11-13 22:56:39', '2019-11-13 22:56:39'),
(69, '8f229c91-3a87-494a-a9c7-6f2fd9c0c467', 2, 'Create', 'App\\Santri', 28, 'App\\Santri', 28, 'App\\Santri', 28, '', 'finished', '', '2019-11-14 20:23:04', '2019-11-14 20:23:04'),
(70, '8f22a35c-015a-4d1e-b6f1-e505e3a54925', 2, 'Update', 'App\\Santri', 28, 'App\\Santri', 28, 'App\\Santri', 28, '', 'finished', '', '2019-11-14 20:42:03', '2019-11-14 20:42:03'),
(71, '8f22a36d-c968-49f7-9bcc-bd8bcfb91466', 2, 'Update', 'App\\Santri', 28, 'App\\Santri', 28, 'App\\Santri', 28, '', 'finished', '', '2019-11-14 20:42:15', '2019-11-14 20:42:15'),
(72, '8f22c9cf-41dc-4f2f-b9ce-3c647314eb69', 2, 'Update', 'App\\Santri', 28, 'App\\Santri', 28, 'App\\Santri', 28, '', 'finished', '', '2019-11-14 22:29:34', '2019-11-14 22:29:34'),
(73, '8f22ca20-2908-43c5-ba05-d2cfb89f3f97', 2, 'Update', 'App\\Santri', 28, 'App\\Santri', 28, 'App\\Santri', 28, '', 'finished', '', '2019-11-14 22:30:27', '2019-11-14 22:30:27'),
(74, '8f22cc53-17ec-44e7-929b-64a51151f158', 2, 'Create', 'App\\Santri', 29, 'App\\Santri', 29, 'App\\Santri', 29, '', 'finished', '', '2019-11-14 22:36:36', '2019-11-14 22:36:36'),
(75, '8f22cd6b-9d40-40ca-a562-9cca6f8cbe7a', 2, 'Update', 'App\\Santri', 28, 'App\\Santri', 28, 'App\\Santri', 28, '', 'finished', '', '2019-11-14 22:39:40', '2019-11-14 22:39:40'),
(76, '8f22ce21-0166-401f-b25f-9f9f288a409d', 2, 'Update', 'App\\Santri', 28, 'App\\Santri', 28, 'App\\Santri', 28, '', 'finished', '', '2019-11-14 22:41:39', '2019-11-14 22:41:39'),
(79, '8f22d664-70c4-4373-8506-0d1f94b5bf96', 3, 'Delete Santri Data', 'App\\Santri', 27, 'App\\Santri', 27, 'App\\Santri', 27, 'a:1:{s:6:\"reason\";s:33:\"anaknya nakal suka main perempuan\";}', 'finished', '', '2019-11-14 23:04:45', '2019-11-14 23:04:45'),
(80, '8f230a6d-88fc-4c23-8fa6-19105d8cfb9b', 3, 'Create', 'App\\Santri', 1, 'App\\Santri', 1, 'App\\Santri', 1, '', 'finished', '', '2019-11-15 01:30:15', '2019-11-15 01:30:15'),
(81, '8f230b4b-d279-480c-aa99-760d30d09ae6', 3, 'Create', 'App\\Santri', 2, 'App\\Santri', 2, 'App\\Santri', 2, '', 'finished', '', '2019-11-15 01:32:41', '2019-11-15 01:32:41'),
(82, '8f230b75-9b87-4b3f-8bd7-0d0094667d72', 3, 'Delete Santri Data', 'App\\Santri', 2, 'App\\Santri', 2, 'App\\Santri', 2, 'a:1:{s:6:\"reason\";s:24:\"Anaknya nakal suka dugem\";}', 'finished', '', '2019-11-15 01:33:08', '2019-11-15 01:33:08');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Jombang', NULL, '2018-11-06 07:45:26', '2018-11-06 07:45:26'),
(2, 'Malang', NULL, '2018-11-07 09:32:16', '2018-11-07 09:32:16'),
(3, 'Surabaya', NULL, '2018-12-17 08:14:21', '2018-12-17 08:14:21');

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

CREATE TABLE `merchants` (
  `id` int(10) UNSIGNED NOT NULL,
  `merchant_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `no_registrasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_desa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_kodepos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pimpinan_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_desa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_kodepos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pemilik_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merchant_category_id` int(11) NOT NULL,
  `pondok_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id`, `merchant_name`, `username`, `password`, `first_name`, `last_name`, `description`, `phone`, `saldo`, `image`, `otp`, `status`, `no_registrasi`, `alamat`, `desa`, `kecamatan`, `kota`, `provinsi`, `fax`, `email`, `website`, `pimpinan_nama`, `pimpinan_alamat`, `pimpinan_desa`, `pimpinan_kecamatan`, `pimpinan_kota`, `pimpinan_provinsi`, `pimpinan_kodepos`, `pimpinan_phone`, `pimpinan_email`, `pemilik_nama`, `pemilik_alamat`, `pemilik_desa`, `pemilik_kecamatan`, `pemilik_kota`, `pemilik_provinsi`, `pemilik_kodepos`, `pemilik_phone`, `pemilik_email`, `merchant_category_id`, `pondok_id`, `token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Graha Alat Tulis', 'graha', '$2y$10$JUbXwzkm6vGpcOoRF0yWi.UD3NLlORF/Gux9s132XvNPTcoTSQxNC', 'Merchant1', 'Keren', '<div>Terlengkap dan termurah!</div>', '0852369741', 0, 'Jv8ri2cYoGZq6QAeNkTdDcFwc3kuLnxqRS3fWWER.png', '269162', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2018-11-07 03:00:08', '2018-11-16 02:36:33'),
(2, 'Steak Warung', 'waroeng', '$2y$10$GpUmj2ZUa/1AoH3buHaq8.hftLEd1366U7L9SJzGIMGvo1FFC8hbC', 'Steak', 'Warung', 'Keren banget', '62963258741', 96020, 'dS4eQXkOA6fmOyXx15cN1548748042.jpg', '744246', 1, NULL, 'pinggir pasar rame', 'arjosari', 'mayangan', 'jember', 'bali', '085698523', 'gue@kazana.id', 'www.merchant.com', 'Djoko', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Juki', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 1, '2G4L64Nnym8mxiNSuVbkU3QXizSsNiW5z', NULL, '2018-11-09 06:25:07', '2019-02-20 08:53:48'),
(3, 'Bebek Slamet', 'bebek', '$2y$10$Wlzgyqm7lEcZZTeW0GtXneHRwwmTdkMRJcC8PG4FExg6RNKIGo5Da', 'Haji', 'Slamet', 'Bebek paling enak', '62852369741', 55000, '', '644071', 1, NULL, 'Juanda 12', 'Jodipan', 'Blimbing', 'Malang', 'Jawa Timur', '362514', 'bebek@kazana.id', 'bebek123.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 2, '', NULL, '2018-11-19 07:30:25', '2019-01-18 08:29:14'),
(4, 'Spesial Sambar', 'sambal', '$2y$10$FP9Cp3A9VqinLoYjZdLAUeoAU9fF6QIEMbzEC61nsA1DCqrmaME7q', 'Hajjah', 'Sumiati', 'Sambl palig pedas', '089876372', 0, '', '875573', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2018-11-19 07:30:25', '2018-11-19 07:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `merchant_categories`
--

CREATE TABLE `merchant_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merchant_categories`
--

INSERT INTO `merchant_categories` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ATK', NULL, '2018-11-07 02:57:03', '2018-11-07 02:57:03'),
(2, 'Kelontong', NULL, '2018-12-17 08:15:00', '2018-12-17 08:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `merchant_password_resets`
--

CREATE TABLE `merchant_password_resets` (
  `id` int(11) NOT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nominals`
--

CREATE TABLE `nominals` (
  `id` int(10) UNSIGNED NOT NULL,
  `barcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nominals`
--

INSERT INTO `nominals` (`id`, `barcode`, `value`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '5000___977904___5be29432cb1e5', 5000, 1, NULL, '2018-11-07 07:28:50', '2018-11-07 07:28:50'),
(2, '10000___793803___5be29d0613025', 10000, 1, NULL, '2018-11-07 08:06:30', '2018-11-07 08:06:30'),
(3, '20000___059242___5be29d0b6794c', 20000, 1, NULL, '2018-11-07 08:06:35', '2018-11-07 08:06:35'),
(4, '50000___974506___5be29d10332eb', 50000, 1, NULL, '2018-11-07 08:06:40', '2018-11-07 08:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0:unread; 1:read',
  `merchant_id` int(11) NOT NULL,
  `santri_id` int(11) NOT NULL,
  `pondok_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nova_pending_trix_attachments`
--

CREATE TABLE `nova_pending_trix_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `draft_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nova_trix_attachments`
--

CREATE TABLE `nova_trix_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `attachable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachable_id` int(10) UNSIGNED DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ortus`
--

CREATE TABLE `ortus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `masih_hidup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penghasilan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ibu_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ibu_masih_hidup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ibu_pendidikan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ibu_pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ibu_penghasilan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kodepos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_telepon_rumah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_telepon_ayah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_telepon_ibu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `santri_id` int(11) NOT NULL,
  `pondok_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ortus`
--

INSERT INTO `ortus` (`id`, `name`, `phone`, `username`, `password`, `otp`, `masih_hidup`, `pendidikan`, `pekerjaan`, `penghasilan`, `ibu_nama`, `ibu_masih_hidup`, `ibu_pendidikan`, `ibu_pekerjaan`, `ibu_penghasilan`, `alamat`, `desa`, `kecamatan`, `kota`, `provinsi`, `kodepos`, `nomor_telepon_rumah`, `nomor_telepon_ayah`, `nomor_telepon_ibu`, `image`, `santri_id`, `pondok_id`, `token`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Ayah Ortu', '62856466619', 'ortuku', '$2y$10$5PBOm0DBQ/R3ppwCnxKikuMmzkSTojnzE64sQsiRvO0LrhA3ZW9ti', '562816', 'hidup', 'smp', 'tani', '1.000.000-2.000.000', 'Ibu Ortu', 'hidup', 'sma', 'wiraswasta', '>2.000.000', 'Nsjdjd', 'Snsnjdjd', 'Bdhdjjdjd', 'Xndjdjjd', 'Bxjsjjs', '60261', '6298112222', '0856466619', '0856194946', 'gCuL8XGqyNU2du30OWzV1549000075.jpg', 4, 1, '1FDEsbUjFQX2rWmxU2UtkZaiCWF9PNyDF', 1, NULL, '2018-11-06 10:03:47', '2019-02-12 00:04:07'),
(2, 'Ortu2', NULL, 'ortu2', '$2y$10$1uPdBCc/I2qKauLHtr5DJOusAmz25MHPhOHcfFlAGnYbiic.WGNPK', '862140', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, '', 1, NULL, '2018-11-13 08:05:47', '2018-12-14 07:17:23'),
(3, 'Bapaknya Tono', NULL, 'tono', '$2y$10$HwknQd8tUcXNNLFypS4sTusC2BcQdTxKiekMIhbe8UsC5F5JY2tNW', '337156', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 2, NULL, 1, NULL, '2018-11-19 07:53:07', '2018-11-19 07:53:07');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `text`, `created_at`, `updated_at`) VALUES
(1, 'Frequently Asked Question', '<div><strong>Bagaimana cara membayar?</strong></div><div>Datang ke kasir<br><br></div><div><strong>Bagaimana cari Top UP?</strong></div><div>Minta baik - baik ke orang tua</div><div><br></div><div><strong>Bagaimana cara Daftar sebagai Merchant?</strong><br>Register via Aplikasi Merchant<br><br></div>', '2018-11-07 03:21:15', '2019-01-04 03:20:38'),
(2, 'Term and Condition', '<div><strong>Syarat masuk Pondok Pesantren</strong><br>Anak Sholeh<br>Mau Belajar</div><div>Mau Berusaha<br><br><strong>Drop out</strong><br>Bila santri melanggar, maka bersedia di keluarkan dari PondokPesantren</div><div><br><strong>Minimal Saldo Top Up</strong><br>Rp. 10.000,-</div>', '2018-11-07 08:02:22', '2019-01-04 03:22:32'),
(3, 'Tutorial', '<ul><li>Login</li><li>Baca FAQ dan Tutorial</li><li>Cara Top Up</li></ul><div><br></div>', NULL, '2019-01-04 03:16:43'),
(4, 'Privacy Policy', '<div>privacy plicy</div>', '2018-12-30 03:11:14', '2018-12-30 03:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_topups`
--

CREATE TABLE `payment_topups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_topups`
--

INSERT INTO `payment_topups` (`id`, `name`, `description`, `image`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'BCA', '<div>Mudah dan Cepat</div>', 'ScA8PSBWfjCslwdUOm6RNlofMpwRYOYirObp5Jcq.png', 1, NULL, '2018-11-07 07:32:49', '2018-11-07 07:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `payment_topup_histories`
--

CREATE TABLE `payment_topup_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_topup_id` int(11) NOT NULL,
  `ortu_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_topup_histories`
--

INSERT INTO `payment_topup_histories` (`id`, `payment_topup_id`, `ortu_id`, `amount`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 500000, NULL, '2019-01-28 00:00:00', NULL),
(2, 1, 1, 250000, NULL, '2019-01-27 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pondoks`
--

CREATE TABLE `pondoks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nspp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ponpes_kepala` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ponpes_alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ponpes_telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ponpes_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yayasan_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yayasan_alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yayasan_telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yayasan_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pondoks`
--

INSERT INTO `pondoks` (`id`, `name`, `phone`, `description`, `image`, `address`, `nspp`, `desa`, `kecamatan`, `kota`, `provinsi`, `fax`, `email`, `website`, `ponpes_kepala`, `ponpes_alamat`, `ponpes_telepon`, `ponpes_email`, `yayasan_nama`, `yayasan_alamat`, `yayasan_telepon`, `yayasan_email`, `city_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Tebu Ireng', '0852369741', '<div>Keren banget</div>', 'BC6zcgaZNAFRH74vNPx2yXjONa9vMWrHl6lB4Gw3.png', 'Jl Pinggir Jombang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2018-11-06 07:49:19', '2018-11-07 08:05:13'),
(2, 'Tambak Beras', '0896325741', 'Pondok Juraga Beras', '', 'Jl Desa Padi no 01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2018-11-19 09:10:32', '2018-11-19 09:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `created_at`, `updated_at`) VALUES
(1, 'root', 'Root', '2018-11-25 00:26:24', '2018-11-25 02:13:31'),
(2, 'admin', 'Admin', '2018-11-25 02:14:18', '2018-11-25 02:14:18'),
(3, 'cabang', 'Cabang', '2018-11-27 07:57:30', '2018-11-27 07:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`role_id`, `permission_slug`, `created_at`, `updated_at`) VALUES
(1, 'assignRoles', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageCity', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageMerchant', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageMerchantCategory', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageNominal', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageNotification', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageOrtu', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'managePage', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'managePaymentTopup', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'managePondok', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageRoles', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageSantri', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageTransaction', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'manageUsers', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewCity', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewMerchant', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewMerchantCategory', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewNominal', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewNotification', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewNova', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewOrtu', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewPage', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewPaymentTopup', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewPondok', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewRoles', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewSantri', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewTransaction', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(1, 'viewUsers', '2018-11-26 00:21:48', '2018-11-26 00:21:48'),
(2, 'manageCity', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'manageMerchant', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'manageMerchantCategory', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'manageNominal', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'manageOrtu', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'managePage', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'managePondok', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'manageSantri', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewCity', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewMerchant', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewMerchantCategory', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewNominal', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewNotification', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewOrtu', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewPage', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewPondok', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewSantri', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewTransaction', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(2, 'viewUsers', '2018-12-06 00:58:31', '2018-12-06 00:58:31'),
(3, 'manageMerchant', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'manageOrtu', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'manageSantri', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewCity', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewMerchant', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewMerchantCategory', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewNominal', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewNotification', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewOrtu', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewPondok', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewSantri', '2018-12-06 00:58:15', '2018-12-06 00:58:15'),
(3, 'viewTransaction', '2018-12-06 00:58:15', '2018-12-06 00:58:15');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `santris`
--

CREATE TABLE `santris` (
  `id` int(10) UNSIGNED NOT NULL,
  `barcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nisn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `no_virtual_account` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_daily_limit` int(100) DEFAULT NULL,
  `transaction_today_total` int(100) DEFAULT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `kelamin` int(11) DEFAULT NULL,
  `anak_ke` int(11) DEFAULT 0,
  `jumlah_saudara` int(11) DEFAULT 0,
  `golongan_darah` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asal_sekolah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asal_sekolah_alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asal_sekolah_desa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asal_sekolah_kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asal_sekolah_kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asal_sekolah_provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_un` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_skhun` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mata_pelajaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_sekolah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diterima_dikelas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_pondok` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_kamar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `punya_saudara_ponpes` tinyint(1) DEFAULT 0,
  `jumlah_saudara_ponpes` int(11) DEFAULT 0,
  `prestasi_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prestasi_tingkat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prestasi_tahun` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sumber_informasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pondok_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_reason` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `santris`
--

INSERT INTO `santris` (`id`, `barcode`, `nisn`, `name`, `description`, `address`, `phone`, `pin`, `image`, `saldo`, `no_virtual_account`, `transaction_daily_limit`, `transaction_today_total`, `tempat_lahir`, `tanggal_lahir`, `kelamin`, `anak_ke`, `jumlah_saudara`, `golongan_darah`, `asal_sekolah`, `asal_sekolah_alamat`, `asal_sekolah_desa`, `asal_sekolah_kecamatan`, `asal_sekolah_kota`, `asal_sekolah_provinsi`, `nomor_un`, `nomor_skhun`, `mata_pelajaran`, `nilai`, `nama_sekolah`, `diterima_dikelas`, `nama_pondok`, `nama_kamar`, `punya_saudara_ponpes`, `jumlah_saudara_ponpes`, `prestasi_nama`, `prestasi_tingkat`, `prestasi_tahun`, `sumber_informasi`, `pondok_id`, `status`, `deleted_at`, `created_at`, `updated_at`, `deleted_reason`) VALUES
(1, '2___1234567890___5dce62172e1cf', '1234567890', 'Andik Firmansyah', '<div>Saya ingin memperdalam ilmu agama.</div>', 'Denanyar Jombang', '085234301234', '$2y$10$v5zyAjez31yAtccSrvWl6e0omNhgwXLfRYqsL7SxPMWHWzXyv9XB6', 'Y8uTlbbkAHEtBGp93kAFjvjhagUfcuax0vJXAnBU.jpeg', 1000000, '889861234567890', 50000, 0, 'Jombang', '2005-11-13', 1, 1, 2, 'o', 'MAN Denanyar Jombang', 'Denanyar Jombang', 'Denanyar', 'Denanyar', 'Denanyar', 'Denanyar', NULL, NULL, NULL, NULL, 'MTS Denanyar', 'a', 'Nur Hidayah Al-Jufri', 'Mawar Hitam', 1, 1000, NULL, NULL, NULL, 'keluarga', 2, 0, NULL, '2019-11-15 01:30:15', '2019-11-15 01:30:15', NULL),
(2, '2___1234567891___5dce62a96b98c', '1234567891', 'Fikri Mustofa', '<div>Fikri Mustofa</div>', 'Jombang', '085234301232', '$2y$10$vgX1q76Uj/Y3KmcedF0tQeSbF/Xi2mCXt0TK5LqT0bZH8hppdBqKy', 'jkGf8kMEmSNW3bZz378B48tiPcYuzfXgaHTCIOb4.jpeg', 500000, '889861234567891', 20000, 0, 'Jombang', '2007-11-10', 1, 1, 2, 'b', 'Man Denanyar', 'Denanyar', 'Denanyar', 'Denanyar', 'Jombang', 'Jawa Timur', NULL, NULL, NULL, NULL, 'MTS Denanyar', 'a', 'Nur Hidayat Al-Jufri', 'Mawar', 1, 1000, NULL, NULL, NULL, 'teman', 2, 2, NULL, '2019-11-15 01:32:41', '2019-11-15 01:33:08', 'Anaknya nakal suka dugem');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `saldo_balance` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `santri_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `pondok_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `note`, `invoice`, `amount`, `saldo_balance`, `status`, `santri_id`, `merchant_id`, `pondok_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'pembayaran sabun', '1P2M181109D1', 5000, NULL, 1, 4, 2, 1, NULL, '2018-11-09 09:41:14', '2018-11-09 09:41:14'),
(2, 'beli indomie', '1P2M181109D2', 2500, NULL, 1, 4, 2, 1, NULL, '2018-11-09 09:43:20', '2018-11-09 09:43:20'),
(3, 'beli indomie goreng', '1P2M181110D3', 2500, NULL, 1, 4, 2, 1, NULL, '2018-11-10 07:49:36', '2018-11-10 07:53:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poster` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pondok_id` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `poster`, `pondok_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'root', 'root@kazana.id', NULL, '$2y$10$fXHPO5bLM0I4mBF3sYPclO9hPsmzJFrbU4CUEA7z.qdScv8NM6uBS', 'CBwLRsLijZtWNU5hoOmZH5iWiuOBjC1V7ZSnpBLn.jpeg', 0, 'JZWpQQdyeskh9WtQ0vRsAvIgcZH3hjNj5Ggl5t4K8ohwOpehINxcEpgNewLi', '2018-11-04 19:57:05', '2019-01-18 00:23:08'),
(2, 'super admin', 'admin@kazana.id', NULL, '$2y$10$/VLaP6PIaNLA.e0cCl3.1uePFsrD/pacitj/HOP4RosNPqKI7556C', NULL, 0, 'oWW10PXfOupBpoKrd3FwTuSKhBpWJiPACybCzRMn6O795kMlskJ8LWzjiLQi', '2018-11-24 19:13:04', '2018-11-24 19:13:04'),
(3, 'admin', 'tambak.beras@kazana.id', NULL, '$2y$10$/otu/Yug.2pq7C5XixeUyuLShls0U0cY0oWwLefBzI2RngnypimQK', NULL, 2, 'IHF8OffGoHiWDjroULyIGwAy8fuDR6739nzuu91jFk1OsA5DTL2oEuNxut9Y', '2018-11-27 00:37:45', '2018-11-27 00:37:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_events`
--
ALTER TABLE `action_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_events_actionable_type_actionable_id_index` (`actionable_type`,`actionable_id`),
  ADD KEY `action_events_batch_id_model_type_model_id_index` (`batch_id`,`model_type`,`model_id`),
  ADD KEY `action_events_user_id_index` (`user_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchant_categories`
--
ALTER TABLE `merchant_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchant_password_resets`
--
ALTER TABLE `merchant_password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_phone_index` (`phone`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nominals`
--
ALTER TABLE `nominals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nova_pending_trix_attachments`
--
ALTER TABLE `nova_pending_trix_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nova_pending_trix_attachments_draft_id_index` (`draft_id`);

--
-- Indexes for table `nova_trix_attachments`
--
ALTER TABLE `nova_trix_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nova_trix_attachments_attachable_type_attachable_id_index` (`attachable_type`,`attachable_id`),
  ADD KEY `nova_trix_attachments_url_index` (`url`);

--
-- Indexes for table `ortus`
--
ALTER TABLE `ortus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_topups`
--
ALTER TABLE `payment_topups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_topup_histories`
--
ALTER TABLE `payment_topup_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pondoks`
--
ALTER TABLE `pondoks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`role_id`,`permission_slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `santris`
--
ALTER TABLE `santris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_events`
--
ALTER TABLE `action_events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `merchants`
--
ALTER TABLE `merchants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `merchant_categories`
--
ALTER TABLE `merchant_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `merchant_password_resets`
--
ALTER TABLE `merchant_password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `nominals`
--
ALTER TABLE `nominals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `nova_pending_trix_attachments`
--
ALTER TABLE `nova_pending_trix_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nova_trix_attachments`
--
ALTER TABLE `nova_trix_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ortus`
--
ALTER TABLE `ortus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_topups`
--
ALTER TABLE `payment_topups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_topup_histories`
--
ALTER TABLE `payment_topup_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pondoks`
--
ALTER TABLE `pondoks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `santris`
--
ALTER TABLE `santris`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
