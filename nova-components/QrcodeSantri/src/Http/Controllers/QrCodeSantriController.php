<?php

namespace Kazana\QrcodeSantri\Http\Controllers;

use Validator;
use App\Santri;
use App\Pondoks;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class QrCodeSantriController extends Controller
{
    /**
     * @param Request $request
     */
    public function getData(Request $request)
    {
        if ( \Auth::user()->pondok_id > 0 )
        {
            $santris = Santri::with(['pondok'])->where('pondok_id', '=', \Auth::user()->pondok_id)->orderByDesc('id')->get();
        }
        else
        {
            $santris = Santri::with(['pondok'])->orderByDesc('id')->get();
        }
        if ( $santris )
        {
            $array = [];
            foreach ( $santris as $k => $v)
            {
                $array[] = $v;
            }

            $data = ['status' => 1, 'data' => $array];
            return response()->json($data);
        }
        else
        {
            return response()->json(['status' => 0]);
        }
    }	
}