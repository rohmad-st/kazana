<?php

namespace Kazana\QrcodeSantri\Http\Controllers;

use Validator;
use App\Notification;
use App\Pondoks;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class NotificationController extends Controller
{
    /**
     * @param Request $request
     */
    public function getData(Request $request)
    {
        if ( \Auth::user()->pondok_id > 0 )
        {
            $santris = Notification::with(['pondok'])->where('pondok_id', '=', \Auth::user()->pondok_id)->orderByDesc('id')->take(10)->get();
        }
        else
        {
            $santris = Notification::with(['pondok'])->orderByDesc('id')->take(10)->get();
        }
        if ( $santris )
        {
            $array = [];
            foreach ( $santris as $k => $v)
            {
                $array[] = $v;
            }

            $data = ['status' => 1, 'data' => $array];
            return response()->json($data);
        }
        else
        {
            return response()->json(['status' => 0]);
        }
    }	
}