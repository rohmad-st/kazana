<?php

namespace Kazana\QrcodeSantri\Http\Middleware;

use Kazana\QrcodeSantri\QrcodeSantri;

class Authorize
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next)
    {
        return resolve(QrcodeSantri::class)->authorize($request) ? $next($request) : abort(403);
    }
}
