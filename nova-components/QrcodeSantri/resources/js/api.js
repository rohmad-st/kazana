export default {
    getData() {
        return window.axios
            .get('/nova-vendor/kazana/qrcode-santri/data', {
                params: { },
            })
            .then(response => response.data);
    },
};
