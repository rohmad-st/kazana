<?php

namespace Kazana\SantriImportPhoto;

use Illuminate\Http\UploadedFile;

class ImportFileReader
{
    /**
     * @var UploadedFile
     */
    protected $file;

    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Allowed mime types.
     *
     * @return string
     */
    public static function mimes()
    {
        return 'zip';
    }

    public function afterRead()
    {
        if (file_exists($this->file->getRealPath())) {
            unlink($this->file->getRealPath());
        }
    }

    public function read(): array
    {
        $original_file_name = $this->file->getClientOriginalName();
        $target_zip_file = storage_path('app/foto/santri_'. $original_file_name);
        copy($this->file->getRealPath(), $target_zip_file);

        $photos_dir = storage_path('app/foto/santri_tmp');
        \Zipper::make($target_zip_file)->extractTo($photos_dir);

        $data = [];

        if ( is_dir($photos_dir))
        {
            $photos = scandir($photos_dir);
            foreach ( $photos as $p )
            {
                if (is_file($photos_dir .'/'. $p))
                {
                    $data[] = $photos_dir .'/'. $p;
                }
            }
        }

        return $data;
    }    

}
