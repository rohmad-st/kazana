<?php

namespace Kazana\SantriImportPhoto;

use App\Santri;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Laravel\Nova\Http\Requests\NovaRequest;

class ImportController
{
    public function handle(NovaRequest $request)
    {
        $fileReader = ImportFileReader::class;

        $data = Validator::make($request->all(), [
            'file' => 'required|file|mimes:'.$fileReader::mimes(),
        ])->validate();
        try {
            $fileReader = new $fileReader($data['file']);
            $data = $fileReader->read();
            $fileReader->afterRead();
        } catch (\Exception $e) {
            Action::danger(__('An error occurred during the import'));
        }

        // remove validation from Nova
        //$this->validateFields($data, $request, $resource);

        DB::transaction(function () use ($data) {
            $this->process($data);
        });

        return Action::message(__('Import photo successful'));
    }

    public function process($data)
    {
        file_put_contents(storage_path('logs/debug.txt'), '');

        if ( count($data) > 0 )
        {
            foreach ( $data as $image )
            {
                $image_basename = basename($image);
                $nisn = preg_replace('/\.(\w+)$/i', '', $image_basename);

                // skip if name is exists
                $check_nisn = Santri::where('nisn', trim($nisn))->first();
                if ( $check_nisn )
                {
                    // log debug
                    file_put_contents(storage_path('logs/debug.txt'), $check_nisn->name ."\n", FILE_APPEND);

                    // copy to santri folder
                    if (copy($image, storage_path('app/public/'. $image_basename)))
                    {
                        // update database
                        $check_nisn->image = $image_basename;
                        $check_nisn->save();
                    }
                }
                else
                {
                    // nisn not found
                }
            }
        }        
    }
}

/* eof */
