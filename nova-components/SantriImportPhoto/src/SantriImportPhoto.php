<?php

namespace Kazana\SantriImportPhoto;

use Laravel\Nova\Card;
use Laravel\Nova\Fields\File;

class SantriImportPhoto extends Card
{
    /**
     * The width of the card (1/3, 1/2, or full).
     *
     * @var string
     */
    public $width = 'full'; //'1/3';

    public function __construct()
    {
        parent::__construct();
        $this->withMeta([
            'fields' => [
                new File('File'),
            ],
        ]);
    }

    /**
     * Get the component name for the element.
     *
     * @return string
     */
    public function component()
    {
        return 'santri_import_photo';
    }
}
