<?php
ini_set('display_errors', '1');
error_reporting(E_ALL);

// include composer autoload
require '../vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

$i = isset($_GET['i']) ? trim($_GET['i'], ' /') : '';
$w = isset($_GET['w']) ? intval($_GET['w']) : 100;
$h = isset($_GET['h']) ? intval($_GET['h']) : 100;

$allowed_resize = ['999x1', 
                  '512x512',
                  '600x600',
                ];
$string_size = $w .'x'. $h;

if ( !in_array($string_size, $allowed_resize))
{
  show404();
}

if ( !$i )
{
  show404();
}
$image_ori = $i;
//$cache_file = str_replace('files/', '../storage/image_caches/', $image_ori);
$cache_file = '../storage/image_caches/'. $string_size .'/'. trim($image_ori, '/ ');

// load from cache
if ( is_file($cache_file))
{
  view_image($string_size, $w, $h, $cache_file);
}

// check folder public
if ( !is_file($i))
{
  // check local storage
  $storage_path = getcwd() . '/../storage/app/public/'. trim($i, '/ ');
  $storage_path = str_replace('//', '/', $storage_path);
  
  if ( is_file($storage_path))
  {
    $i = $storage_path;
  }
  // image not found
  else
  {
    show404();
  }
}

check_create_dir($cache_file);

// and you are ready to go ...
view_image($string_size, $w, $h, $i, $cache_file);

function view_image($string_size, $w, $h, $i, $cache_file = null)
{
    if ( $string_size == '999x1' )
    {
      $image = Image::make($i);
    }
    else
    {
      //$image = Image::make($i)->resize($w, $h);
      $image = Image::make($i)->fit($w, $h);
    }

    if ( $cache_file !== null )
    {
        // cache image
        $image->save($cache_file);
    }
    
    $browserCache = 60*60*24*7;
    $lastModified = gmdate('D, d M Y H:i:s', time()).' GMT';
    header('Cache-Control: public, max-age='. $browserCache);
    header('Expires: '.gmdate('D, d M Y H:i:s', time()+$browserCache).' GMT');
    header("Etag: ". md5($i . time()));
    echo $image->response();
    die();
}

function show404() {
    header("HTTP/1.0 404 Not Found");
    echo "404 Not Found.\n";
    die();  
}

function check_create_dir($cache_file) {
    $dir_dir = dirname($cache_file);
    if ( !is_dir($dir_dir))
    {
        @mkdir($dir_dir, 0777, true);
    }
}
/* eof */