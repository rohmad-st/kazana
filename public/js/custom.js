(function() {
	axios.get('/nova-vendor/kazana/qrcode-santri/notification').then(response => {
		if (response.data.status == 1)
		{
			var textnode = document.createTextNode("10");
			//document.getElementById("notifHeader").appendChild(textnode); 

			var notifImage = document.getElementById("notifHeaderImg");
			notifImage.onclick = function(){
				var notifBody = document.getElementById("notifHeaderBody");
				notifBody.style.display = 'inline';
			};

			for(var i = 0; i < response.data.data.length; i++) {
			    var obj = response.data.data[i];

				var node = document.createElement("li");
				var anode = document.createElement("a");
				var textnode = document.createTextNode(obj.message);
				anode.appendChild(textnode);
				anode.setAttribute("class", "block no-underline text-90 hover:bg-30 p-3");
				anode.href = "/admin/resources/notifications";
				node.appendChild(anode);

				var divnode = document.createElement("br");
				var textnode = document.createTextNode(obj.created_at);
				anode.appendChild(divnode);
				anode.appendChild(textnode);

				document.getElementById("notifHeader").appendChild(node); 
			}
		}
	});
})();

document.addEventListener("click", function(evt) {
	var checkNotifBody = document.getElementById("notifHeaderBody");
	var checkNotifImg = document.getElementById("notifHeaderImg"),
	targetElement = evt.target;

	do {
        if (targetElement == checkNotifImg || targetElement == checkNotifBody) {
            // This is a click inside. Do nothing, just return.
            return;
        }
        // Go up the DOM
        targetElement = targetElement.parentNode;
    } while (targetElement);	

	if (checkNotifBody.style.display == 'inline')
	{
		checkNotifBody.style.display = 'none';
	}
});

/* eof */