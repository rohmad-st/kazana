@extends('topup.layout')

@section('content')
<div class="topup-head">
	ATM
</div>

<div class="topup-body">
	<div class="list-head" id="topup-detail">
		<div class="topup-img-left">
			<img src="/custom/img/atm_mandiri.png"/>
		</div>
		<div class="topup-text-right">
			MANDIRI
		</div>
		<div class="topup-no-akun-virtual">
			NO. AKUN VIRTUAL<br/>
			<input type="text" name="clipcopy" id="clipcopy" value="89148{{ $phone }}" readonly="readonly">
			<div class="topup-salin-va"><a href="#" onclick="javascript: myClipCopy(); return false;">SALIN</a></div>
		</div>

		<div class="topup-instruction">Instruksi</div>
		<div class="topup-bullet">
			<ul>
				<li>Masukkan kartu ATM dan PIN MANDIRI Anda</li>
				<li>Pilih menu <strong>Bayar/Beli > Lainnya > Lainnya > pilih E-Commerce</strong></li>
				<li>Masukkan <strong>kode perusahaan untuk KAZANA: 89148</strong></li>
				<li>Masukkan <strong>Nomor Induk Siswa Nasional: {{ $phone }}</strong></li>
				<li>Masukkan <strong>nominal top-up</strong></li>
				<li>Ikuti instruksi untuk menyelesaikan transaksi</li>
			</ul>
		</div>


		<div class="topup-instruction-bottom">Catatan</div>
		<div class="topup-bottom">
			<ul>
				<li>Minimum top-up Rp20.000</li>
				<li>Gratis biaya top-up</li>
			</ul>
		</div>		
	</div>
</div>

@endsection