@extends('topup.layout')

@section('content')
<div class="topup-head">
	Pilih Metode
</div>

<div class="topup-body">
	<div class="list-head">
		<div class="topup-img-left">
			<a href="/topup/atm"><img src="/custom/img/atm.png" width="40" /></a>
		</div>
		<div class="topup-text-right">
			<a href="/topup/atm">ATM</a>
		</div>
	</div>

	<div class="list-head">
		<div class="topup-img-left">
			<a href="/topup/mbanking"><img src="/custom/img/mbanking.png" width="40" /></a>
		</div>
		<div class="topup-text-right">
			<a href="/topup/mbanking">Internet / Mobile Banking</a>
		</div>
	</div>
</div>
@endsection