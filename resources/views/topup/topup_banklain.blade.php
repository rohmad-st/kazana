@extends('topup.layout')

@section('content')
<div class="topup-head">
	Internet/ Mobile Banking
</div>

<div class="topup-body">
	<div class="list-head" id="topup-detail">
		<div class="topup-text-right">
			BANK LAIN
		</div>

		<div class="topup-instruction">Instruksi</div>
		<div class="topup-bullet">
			<ul>
				<li>Login ke <strong>Mobile / Internet Banking</strong> Anda yang tergabung dalam jaringan berikut: <br/>
					<img src="/custom/img/bank_lain.png"/></li>
				<li>Pilih menu <strong>Transfer</strong></li>
				<li>Pilih menu <strong>Transfer ke Bank Lain</strong></li>
				<li>Pilih <strong>Bank Nobu</strong>/ masukkan kode bank 123</li>
				<li>Masukkan 9+ nomor ponsel Anda:<br/>
					<strong>9 08xxxxxxxxx</strong></li>
				<li>Masukkan <strong>Nominal Transfer</strong></li>
				<li>Ikuti instruksi untuk menyelesaikan transaksi</li>
			</ul>
		</div>


		<div class="topup-instruction-bottom">Catatan</div>
		<div class="topup-bottom">
			<ul>
				<li>Biaya top-up disesuaikan dengan syarat & ketentuan Bank terkait</li>
			</ul>
		</div>		
	</div>
</div>

@endsection