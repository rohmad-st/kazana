<!DOCTYPE html>
<html lang="en" class="h-full font-sans antialiased">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Kazana</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="/vendor/nova/app.css?id=fdf84a70c621d6f1fa27">
    <style>
    .topup-head {
    	background: #f4f7fa;
    	padding: 10px;
    }
    .topup-body {
        margin: 0 20px;
    }
    .topup-body a {
        text-decoration: none;
        color: #000;
        font-size: 20px;
    }
    .list-head {
    	background: #ffffff;
    	padding: 10px;
    	margin: 20px 0;
    	border: 2px solid #f4f7fa;
        border-radius: 15px;
        float: left;
        width: 100%;
    }
    .topup-img-left {
        float: left;
        width: 80px;
    }
    .topup-text-right {
    	padding-top: 5px;
    }
    .topup-instruction {
    	clear: both;
    	padding: 10px 0;
    }
	.topup-bullet ul, .topup-bottom ul {
	  list-style: none; /* Remove default bullets */
	  padding: 0 15px;
	}

	.topup-bullet ul li::before, .topup-bottom  ul li::before{
	  content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
	  color: #4CAF50; /* Change the color */
	  font-weight: bold; /* If you want it to be bold */
	  display: inline-block; /* Needed to add space between the bullet and the text */ 
	  width: 1em; /* Also needed for space (tweak if needed) */
	  margin-left: -1em; /* Also needed for space (tweak if needed) */
	}
	.topup-bottom, .topup-instruction-bottom {
		font-size: 12px;
	}
	.topup-bottom {
		position: absolute;
		bottom: 75px;
	}
	.topup-instruction-bottom {
		position: absolute;
		bottom: 105px;
	}
    .topup-no-akun-virtual {
        width: 100%;
        clear: both;
        padding: 15px 0;
        position: relative;
    }
    .topup-salin-va {
        position: absolute;
        right: 0;
        top: 20px;
        border: 2px solid #4CAF50;
        border-radius: 5px;
        padding: 0 8px;
    }
    .topup-salin-va a {
        font-size: 12px;
    }
    #clipcopy {
        font-weight: bold;
    }
    </style>
</head>
<body>
@yield('content')
<script type="text/javascript">//<![CDATA[ 
function resize()
{
    var heights = window.innerHeight - 115;
    document.getElementById("topup-detail").style.height = heights + "px";
}
resize();
window.onresize = function() {
    resize();
};

function myClipCopy() {
  /* Get the text field */
  var copyText = document.getElementById("clipcopy");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  // alert("Copied the text: " + copyText.value);
}

//]]>  
</script> 
</body>
</html>