@extends('topup.layout')

@section('content')
<div class="topup-head">
	Internet/ Mobile Banking
</div>

<div class="topup-body">
	<div class="list-head">
		<div class="topup-img-left">
			<a href="/topup/mbca"><img src="/custom/img/atm_bca.png" width="70" /></a>
		</div>
		<div class="topup-text-right">
			<a href="/topup/mbca">BCA</a>
		</div>
	</div>

	<div class="list-head">
		<div class="topup-img-left">
			<a href="/topup/mmandiri"><img src="/custom/img/atm_mandiri.png"/></a>
		</div>
		<div class="topup-text-right">
			<a href="/topup/mmandiri">MANDIRI</a>
		</div>
	</div>

	<!--<div class="list-head">
		<div class="topup-text-right">
			<a href="/topup/bank-lain">BANK LAIN</a>
		</div>
	</div>-->

</div>


@endsection