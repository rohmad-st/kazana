@extends('topup.layout')

@section('content')
<div class="topup-head">
	ATM
</div>

<div class="topup-body">
	<div class="list-head">
		<div class="topup-img-left">
			<a href="/topup/bca"><img src="/custom/img/atm_bca.png" width="70" /></a>
		</div>
		<div class="topup-text-right">
			<a href="/topup/bca">BCA</a>
		</div>
	</div>

	<div class="list-head">
		<div class="topup-img-left">
			<a href="/topup/mandiri"><img src="/custom/img/atm_mandiri.png"/></a>
		</div>
		<div class="topup-text-right">
			<a href="/topup/mandiri">MANDIRI</a>
		</div>
	</div>

</div>


@endsection