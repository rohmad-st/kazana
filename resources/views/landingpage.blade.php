<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tambakberas - Pondok Pesantren Bahrul Ulum</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('landingpage/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{ asset('landingpage/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('landingpage/vendor/simple-line-icons/css/simple-line-icons.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

  <!-- Plugin CSS -->
  <link rel="stylesheet" href="{{ asset('landingpage/device-mockups/device-mockups.min.css') }}">

  <!-- Custom styles for this template -->
  <link href="{{ asset('landingpage/css/new-age.css') }}" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Tambakberas</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#page-top">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#visi">Visi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#misi">Misi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#kontak">Kontak</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <section class="cta">
    <div class="cta-content">
      <div class="container">
        <h2>Yayasan<br>Pondok Pesantren<br>Bahrul Ulum</h2>
        <a href="#visi" class="btn btn-outline btn-xl js-scroll-trigger">Pelajari Lebih Lanjut</a>
      </div>
    </div>
    <div class="overlay"></div>
  </section>

  <section class="visi bg-primary text-center" id="visi">
    <div class="container">
      <div class="row">
        <div class="col-md-8 mx-auto">
          <h2 class="section-heading">Visi</h2>
		  <hr>
		  <p style="margin-top:20px;">“ Menjadikan Tambak beras sebagai pusat peradaban Islam yang berfungsi sebagai penyeimbang segala peri kehidupan umat manusia, hingga mampu membentuk masyarakat aman, damai, sejahtera. ”</p>
        </div>
      </div>
    </div>
  </section>

  <section class="misi" id="misi">
    <div class="container">
      <div class="section-heading text-center">
        <h2>Misi</h2>
		<hr>
      </div>
      <div class="row">
        <div class="col-lg-12 my-auto">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-4">
                <div class="feature-item">
                  <i class="icon-check text-primary"></i>
                  <p class="text-muted">Menciptakan manusia yang beriman dan bertaqwa kepada Allah serta memiliki rasa tanggung jawab mengembangkan dan menyebarkan ajaran Islam Ahlussunnah Wal Jama’ah.</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="feature-item">
                  <i class="icon-check text-primary"></i>
                  <p class="text-muted">Melahirkan manusia yang berakhlaq mulia, dan memiliki rasa tanggung jawab sosial terhadap kemashlahatan umat.</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="feature-item">
                  <i class="icon-check text-primary"></i>
                  <p class="text-muted">Melahirkan manusia yang cakap, trampil, mandiri, memiliki kemampuan keilmuan dan mampu menerapkan serta mengembangkan ilmu pengetahuan dan ketrampilan yang ada pada dirinya dan lingkungannya.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="kontak" id="kontak">
      <div class="container text-center">
		  <h2 style="padding-top: 50px;">Kontak</h2>
		  <hr>
			<div class="row">
			<div class="col-lg-6 mt-5">
				<span style="font-weight: 500;"><i class="fa fa-map-marker"></i> ALAMAT</span><br>
				<p style="margin-top: 10px">Jl. Kyai Haji Wahab Hasbullah No.80, Tambak Rejo, Kec. Jombang, Kabupaten Jombang, Jawa Timur 61451</p>
				<span style="font-weight: 500;"><i class="fa fa-phone"></i> NO. TELEPHONE</span><br>
				<p style="margin-top: 5px">(0321) 869955</p>			
				<span style="font-weight: 500;"><i class="fa fa-envelope"></i> ALAMAT EMAIL</span><br>
				<p style="margin-top: 5px">yayasan@tambakberas.com</p>
				<span style="font-weight: 500;"><i class="fa fa-globe"></i> OFFICIAL WEBSITE</span><br>
				<p style="margin-top: 5px"><a href="https://www.tambakberas.com/">www.tambakberas.com</a></p>
			
			</div>
			<div class="col-lg-6 my-auto">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15986.87340711095!2d112.22561584038912!3d-7.530036756834683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e783fe36b45d723%3A0x31d5cd8bf59b132e!2sYayasan+Pondok+Pesantren+Bahrul+Ulum+Tambakberas!5e0!3m2!1sid!2sid!4v1539867274245" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>

			</div>
		  </div>
      </div>
  </section>

  <footer>
    <div class="container">
      <p>&copy; KAZANA - Tambak Beras 2019. All Rights Reserved.</p>
      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="#beranda">Beranda</a>
        </li>
        <li class="list-inline-item">
          <a href="#visi">Visi</a>
        </li>
        <li class="list-inline-item">
          <a href="#misi">Misi</a>
        </li>
        <li class="list-inline-item">
          <a href="#kontak">Kontak</a>
        </li>
      </ul>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('landingpage/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('landingpage/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Plugin JavaScript -->
  <script src="{{ asset('landingpage/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{ asset('landingpage/js/new-age.min.js') }}"></script>

</body>

</html>
