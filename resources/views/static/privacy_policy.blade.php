@extends('static.layout')

@section('content')

<div id="app">
<el-card class="box-card">
  <div slot="header" class="clearfix">
	<el-input
	  placeholder="Cari..."
	  v-model="input10"
	  clearable>
	</el-input>    
  </div>
  <div class="text item" v-html="highlight()"></div>
</el-card>
</div>

@endsection

@push('styles')
<style>
  .text {
    font-size: 14px;
  }
  .subtext {
  	color: #AAAAAA;
  	font-size: 12px;
  }

  .item {
    margin-bottom: 18px;
  }

  .clearfix:before,
  .clearfix:after {
    display: table;
    content: "";
  }
  .clearfix:after {
    clear: both
  }

  .box-card {
    width: 100%;
  }

  .highlightText {
      background: yellow;
  }  
</style>
@endpush

@push('scripts')
  <script>
    new Vue({
      el: '#app',
      data: function() {
        return { 
        	visible: false ,
        	input10: '',
          dataText: '{!! $privacy_policy->text !!}'
        }
      },
      methods: { 
        highlight() {
          if(!this.input10) {
              return this.dataText;
          }

          var skipes = ['strong', 'img', 'div', 'src'];
          if(this.input10.length <= 2 || skipes.indexOf(this.input10) > -1) {
              return this.dataText;
          }

          return this.dataText.replace(new RegExp(this.input10, "gi"), match => {
              return '<span class="highlightText">' + match + '</span>';
          });
        }
      }
    })
  </script>
@endpush
