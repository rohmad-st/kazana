<div id="notifHeaderContainer" class="relative h-9 flex ml-auto items-center" style="right: 20px">
	<img id="notifHeaderImg" src="https://placeimg.com/100/100/tech" class="rounded-full w-8 h-8 mr-3"/>

	<div id="notifHeaderBody" class="dropdown-menu absolute z-50 select-none dropdown-menu-right">
		<svg width="46px" height="23px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 44 22" class="absolute dropdown-arrow z-50 dropdown-arrow-right"><defs><path id="a" d="M0 0h44v22H0z"></path><path id="c" d="M2 22L22 2l20 20z"></path></defs><g fill="none" fill-rule="evenodd"><mask id="b" fill="#fff"><use xlink:href="#a"></use></mask><g mask="url(#b)"><use fill="#FFF" xlink:href="#c"></use><path stroke="#CCD4DB" d="M.793 22.5L22 1.293 43.207 22.5H.793z"></path></g></g></svg>

		<div class="z-40 overflow-hidden bg-white border border-60 shadow rounded-lg" style="width: 400px;">
			<ul id="notifHeader" class="list-reset">
			</ul>
		</div>
	</div>
</div>

<!--
<dropdown-trigger class="h-9 flex items-center" slot-scope="{toggle}" :handle-click="toggle">
  	<img src="https://placeimg.com/100/100/tech" class="rounded-full w-8 h-8 mr-3"/>
</dropdown-trigger>

<dropdown-menu slot="menu" width="200" direction="rtl">
    <ul class="list-reset">
        <li>
            <a href="/admin/resources/notifications" class="block no-underline text-90 hover:bg-30 p-3">
                See All
            </a>
        </li>
    </ul>
</dropdown-menu>
-->